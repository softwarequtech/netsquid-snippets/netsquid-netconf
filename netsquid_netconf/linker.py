"""Linker interface and ComponentLinker implementation."""

from abc import ABC, abstractmethod

from netsquid.nodes.network import Node


class Linker(ABC):
    """Abstract base class for NetConf linkers."""

    @abstractmethod
    def link(self, object_dict, conf_dict):
        """Link the objects created by the builders.

        Parameters
        ----------
        object_dict : dict
            Full dictionary of objects created by the builder.
        conf_dict : dict
            The configuration for this linker to process.
        """
        raise NotImplementedError()


class ComponentLinker(Linker):
    """Linker for NetSquid :class:`~netsquid.components.component.Component`\\s.
    Assumes all connections are made via :class:`~netsquid.components.component.Port`\\s.
    Uses the keyword `ports` within a component configuration.
    The keys of the item in the `ports` dictionary must be the names of the ports of the component itself
    that are used to connect it to another component.
    Next, its values should contain the keyword `remote` that specifies the name of the component it should connect to
    as well as the keyword `port` that specifies the port on the remote component.
    If the `ports` keyword is specified in both components that are to be connected, it is verified
    whether the configuration agrees.
    Can also be used to recursively link subcomponents within a component.

    Example
    -------
    .. code-block:: yaml

        # Create three nodes by using the `node` anchor specified somewhere else
        # By default each node has two ports A and B
        components:
          Node0:
            << : *node

          Node1:
            << : *node

            # Used for mutual verification of connections that are specified below
            ports:
              A:
                remote: Connection0
                port: B
              B:
                remote: Node2
                port: A

          Node2:
            <<: *node

            # Connect port A of this node (`Node2`) to port B of node `Node1`.
            ports:
              A:
                remote: Node1
                port: B

          # Create a connection, which has ports A and B by default
          Connection0:
            << : *fibre

            # Connect port A of the connection to port B of Node0
            # and connect port B of the connection to port A of Node1
            ports:
              A:
                remote: Node0
                port: B
              B:
                remote: Node1
                port: A
    """

    @staticmethod
    def _get_port(port_name, comp):
        if port_name not in comp.ports:
            raise ValueError("Component {} does not have port {}"
                             .format(comp.name, port_name))
        return comp.ports[port_name]

    @staticmethod
    def _get_remote_comp(remote_name, comp, peer_comps):
        # Is it a peer component?
        if remote_name in peer_comps:
            return peer_comps[remote_name]

        # If not, is it a sub-component?
        if remote_name in comp.subcomponents:
            return comp.subcomponents[remote_name]

        raise ValueError(
            "Component {}: remote {} is not a peer or sub-component"
            .format(comp.name, remote_name)
        )

    @staticmethod
    def _get_remote_comp_conf(remote_name, comp_name, comp_conf, peer_comps_conf):
        if remote_name in peer_comps_conf:
            return peer_comps_conf[remote_name]

        if remote_name in comp_conf["components"]:
            return comp_conf["components"][remote_name]

        raise ValueError(
            "Component {}: "
            "remote {} is not configured as a peer or sub-component"
            .format(comp_name, remote_name)
        )

    @staticmethod
    def _verify_remote_port_conf(comp_name, port_name, port_conf, remote_port_conf):
        if remote_port_conf["remote"] != comp_name or \
           remote_port_conf["port"] != port_name:
            raise ValueError(
                "Component {}, port {}: "
                "remote port {} on component {} does not agree "
                "that it is the remote"
                .format(comp_name, port_name, port_conf["port"], port_conf["remote"])
            )

    def _recursive_link(self, object_dict, conf_dict):
        for comp_name, comp_conf in conf_dict.items():
            if "ports" not in comp_conf:
                continue

            for port_name, port_conf in comp_conf["ports"].items():

                # Get the component and its port
                comp = object_dict[comp_name]
                port = self._get_port(port_name, comp)

                # If port already connected, skip
                if port.is_connected:
                    continue

                # Find the remote component and its port
                remote_comp = self._get_remote_comp(
                    port_conf["remote"], comp, object_dict,
                )
                remote_port = self._get_port(port_conf["port"], remote_comp)

                # Find the configuration for the remote port (if it exists). If
                # it exists, it must agree that this is the port it wants to
                # connect to.
                remote_comp_conf = self._get_remote_comp_conf(remote_comp.name, comp_name, comp_conf, conf_dict)
                remote_port_conf = remote_comp_conf.get(remote_port.name)

                if remote_port_conf is not None:
                    self._verify_remote_port_conf(comp_name, port_name, port_conf, remote_port_conf)

                # Finally, connect
                port.connect(remote_port)

            # Recursively descend into sub-components
            if "components" in comp_conf:
                assert comp.subcomponents
                self._recursive_link(comp.subcomponents, comp_conf["components"])

    def link(self, object_dict, conf_dict):
        if "components" not in object_dict:
            return None

        self._recursive_link(object_dict["components"], conf_dict["components"])


class NetworkLinker(Linker):
    """Linker that fills in the empty :class:`~netsquid.nodes.network.Network` created by created by
    :class:`~netsquid_netconf.builder.NetworkBuilder`, by adding all the
    :class:`~netsquid.nodes.node.Node`\\s created by other builders and subsequently adds and links any
    :class:`~netsquid.nodes.connections.Connection`\\s  created by other builders to these
    :class:`~netsquid.nodes.node.Node`\\s.
    Looks for the keyword `connect_to` in the configuration of a :class:`~netsquid.nodes.connections.Connection`,
    after which the syntax is based on adding :class:`~netsquid.nodes.connections.Connection`\\s to a
    :class:`~netsquid.nodes.network.Network`.
    Adding a :class:`~netsquid.nodes.connections.Connection` therefore requires a user to specify two
    :class:`~netsquid.nodes.node.Node`\\s with keywords `node1` and `node2`, together with the respective
    :class:`~netsquid.components.component.Port` names `port_name_node1` and `port_name_node2`.
    Optionally, a label can be specified (using the keyword `label`) in order to connect multiple
    :class:`~netsquid.nodes.connections.Connection`\\s to the same :class:`~netsquid.nodes.node.Node`.

    For more info on how to use, see `./docs/tutorial/components_and_networks.rst`.

    Example
    -------
    .. code-block:: yaml

        # Create two nodes by using the `node` anchor specified somewhere else
        # By default each node has two ports A and B
        components:
          Node0:
            << : *node

          Node1:
            << : *node

          # Create a fibre wrapped in a connection and link it to the two nodes
          FibreConnection:
            << : *fibre

            connect_to:
              node1: Node0
              node2: Node1
              port_name_node1: B
              port_name_node2: A
              label: fibre

    """

    def link(self, object_dict, conf_dict):
        if conf_dict.get("components", None) is None or conf_dict.get("network", None) is None:
            return

        object_comp = object_dict["components"]  # dictionary holding all the components objects

        # Retrieve the Network instantiated by NetworkBuilder and add all nodes from the components
        network = object_dict["network"]
        network.add_nodes([obj for obj in object_comp.values() if isinstance(obj, Node)])

        # Then add all the Connections to the Network
        for comp_name, comp_conf in conf_dict["components"].items():
            if "connect_to" not in comp_conf:
                continue

            # Add Connection itself to the dictionary and pass to Network
            comp_conf["connect_to"]["connection"] = object_comp[comp_name]
            network.add_connection(**comp_conf["connect_to"])
