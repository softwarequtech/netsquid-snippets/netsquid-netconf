"""Builder interface and ComponentBuilder implementation."""

from abc import ABC, abstractmethod

from netsquid.components.component import Component
from netsquid.nodes import Network
from netsquid.nodes.node import Node


class Builder(ABC):
    """Abstract base class for NetConf builders."""

    @abstractmethod
    def key(self):
        """Keyword for the configuration subset handled by this builder."""
        raise NotImplementedError()

    @abstractmethod
    def build(self, config_dict):
        """Build the objects identified in the configuration.

        Parameters
        ----------
        config_dict : dict
            The configuration for this builder to process.

        Returns
        -------
        collection
            A collection of objects. Usually a list or dict.
        """
        raise NotImplementedError()


class TypeBuilder(Builder):
    """Specialisation of Builder for type-based building."""

    @abstractmethod
    def types(self):
        """The name to class mapping to be used by the builder."""
        raise NotImplementedError()

    @abstractmethod
    def add_type(self, name, new_type):
        """Add a new name to class mapping to this builder class.

        Parameters
        ----------
        name : str
            The name of the new type.
        new_type : Class
            The class to use for instantiating the new type.
        """
        raise NotImplementedError()


class ComponentBuilder(TypeBuilder):
    """Builder for NetSquid :class:`~netsquid.components.component.Component`\\s
    that are specified in a `.yaml` configuration file.
    Only the data with the keyword `components` is parsed.
    The `type` field is used to refer to the Python class that needs to be instantiated.
    Any of the items in the `properties` field will be unpacked and passed to the constructor of the component.
    The name of the component in the configuration file will be passed as the name
    of the :class:`~netsquid.components.component.Component`.
    If the `components` field is used within a component, any subcomponents will be recursively added to the
    :class:`~netsquid.components.component.Component`.

    Example
    -------
    .. code-block:: yaml

        # Create a Node with name `Node0`, which has two Ports A and B and a QuantumMemory called `QMem`
        # with 10 memory positions
        # Note that the `quantum_memory` type must first be added to the ComponentBuilder with `add_type()`
        components:
          Node0:
            type: node
            properties:
              port_names:
                - A
                - B
            components:
              QMem:
                type: quantum_memory
                properties:
                  num_positions: 10
    """

    # Initialise with built-in NetSquid classes
    TYPES = {
        "node": Node,
    }

    def key(self):
        return "components"

    def types(self):
        return self.TYPES

    def add_type(self, name, new_type):
        assert issubclass(new_type, Component)
        self.TYPES[name] = new_type

    def build(self, config_dict):
        components = {}

        for name, comp in config_dict.items():
            # Extract properties if any
            properties = {}

            if "properties" in comp:
                properties = comp["properties"]

            # Create the component object
            if "type" not in comp:
                raise RuntimeError("Components must have a 'type'")
            component = self.TYPES[comp["type"]](name, **properties)

            # Create and add its sub-components recursively
            if "components" in comp:
                for sub_comp in self.build(comp["components"]).values():
                    component.add_subcomponent(sub_comp)

            components[name] = component

        return components


class NetworkBuilder(Builder):
    """Builder that creates an empty :class:`netsquid.nodes.network.Network` with the name specified in the yaml file.
    The network will be filled in by :class:`~netsquid_netconf.linker.NetworkLinker`."""

    def key(self):
        return "network"

    def build(self, config_dict):
        # Instantiate empty Network with its name (config_dict only holds the network name)
        network = Network(name=config_dict)
        return network
