"""Network configuration module."""

import logging
import os
import yaml

from copy import deepcopy
from flatten_dict import flatten
from itertools import product, chain
from numpy import linspace

logger = logging.getLogger(__name__)


class NetworkConfiguration:
    """Represents a single configuration of the network. Can create configuration files and networks.

    This class can be thought of as a recipe for a network configuration. It contains the `config` method to obtain
    the recipe as a dictionary and the `objects` method to execute the recipe and build the actual network
    (i.e., create objects using specified :class:`netsquid_netconf.builder.Builder`\\s and connect them using
    :class:`netsquid_netconf.linker.Linker`\\s).

    In the recipe, there can still be some free parameters that need to be specified each time `config` or `objects`
    is called. These free parameters should be specified in the configuration dictionary passed upon construction
    of this object using the PARAM special keyword. The `parameters` method of this class can be used to identify
    which (if any) free parameters there are.

    Parameters
    ----------
    config : dict
        Dictionary with all the configuration parameters, following the same structure as YAML config files.
        May only implement the PARAM special keyword.
    builders : list of :class:`~netsquid_netconf.builder.Builder` objects.
        Builders for the provided configuration.
    linkers : list of :class:`~netsquid_netconf.linker.Linker` objects.
        Linkers for the provided configuration.

    """

    def __init__(self, config, builders, linkers):
        self._raw_config = config
        self._builders = builders
        self._linkers = linkers

    @property
    def parameters(self):
        """Set of parameters specified by the PARAM keyword.

        The values of these parameters need to be specified each time a configuration dictionary or network is created
        using this class, i.e., each time :meth:`~netsquid_netconf.netconf.NetworkConfiguration.config` or
        :meth:`~netsquid_netconf.netconf.NetworkConfiguration.objects` is called.

        """
        parameter_names = []
        for path, parameter_name in ((keys, value) for keys, value in flatten(self._raw_config).items()
                                     if keys[-1] == "PARAM"):
            parameter_names.append(parameter_name)
        return set(parameter_names)

    def _check_parameters(self, **kwargs):
        """Check if the specified keyword parameters correspond to the parameters specified using the PARAM keyword."""
        if self.parameters != set(kwargs.keys()):
            raise ValueError(f"Incorrect parameter set.\nParameters that should be specified:\n{self.parameters}\n."
                             f"Parameters that were specified:\n{set(kwargs.keys())}.")

    def config(self, save_path=None, **kwargs):
        """Create a network-configuration dictionary in which all PARAM special keywords are filled in.

        Parameters
        ----------
        save_path : str or None
            Path to where the created configuration should be saved as a YAML file
            (".yaml" suffix is automatically appended if it is not included in save_path).
            If None (default), it is not saved.
        **kwargs
            All the parameters as specified by the PARAM special keyword in the config file given at construction should
            be given values here by passing them as keyword arguments.
            A list of them can be found using :attr:`~netsquid_netconf.netconf.NetworkConfiguration.parameters`.
            If the PARAM keyword is not used, no parameters should be passed.


        Returns
        -------
        dict
            Network-configuration dictionary where the PARAM keywords are replaced by the values given to the function.

        """
        self._check_parameters(**kwargs)
        new_config = deepcopy(self._raw_config)
        for path, parameter_name in ((keys, value) for keys, value in flatten(self._raw_config).items()
                                     if keys[-1] == "PARAM"):
            _nested_dict_set(nested_dict=new_config, set_path=path[:-1], value=kwargs[parameter_name])
        if save_path is not None:
            if save_path[-5:] != ".yaml":
                save_path += ".yaml"
            yaml.dump(new_config, open(save_path, "w"))
        return new_config

    def objects(self, **kwargs):
        """Create and link the objects that make up the network configuration.

        This method uses the specified builders and linkers to translate the configuration dictionary
        into an actual network made up of connected objects.

        Parameters
        ----------
        **kwargs
            All the parameters as specified by the PARAM special keyword in the config file given at construction should
            be given values here by passing them as keyword arguments.
            A list of them can be found using :attr:`~netsquid_netconf.netconf.NetworkConfiguration.parameters`.
            If the PARAM keyword is not used, no parameters should be passed.

        Returns
        -------
        objects : dict
            Dictionary where the key-value pairs are given by the keys of the builders and their respective output.

        """
        self._check_parameters(**kwargs)
        return _get_objects(config=self.config(**kwargs), builders=self._builders, linkers=self._linkers)

    @property
    def builders(self):
        """List of builders used to construct objects in this network configuration."""
        return self._builders

    @property
    def linkers(self):
        """List of linkers used to link objects in this network configuration."""
        return self._linkers

    @property
    def raw_config(self):
        """The raw config dictionary with possibly some free, undefined parameters (i.e., the PARAM special keyword).
        This is simply the configuration dictionary as passed upon construction of the object."""
        return self._raw_config


class Loader(yaml.SafeLoader):
    """Loader class to allow for !include statement in config."""

    def __init__(self, stream):
        self._root = os.path.split(stream.name)[0]
        super(Loader, self).__init__(stream)

    def include(self, node):
        """Process and include statement."""
        filename = os.path.join(self._root, self.construct_scalar(node))

        with open(filename, 'r') as file_handle:
            return yaml.load(file_handle, Loader)


def netconf_generator(config_file, builders, linkers):
    """Use builder(s) and linker(s) to create a generator of :class:`~netsquid_netconf.netconf.NetworkConfiguration`.

    Example usage:

    .. code-block:: python

        # initialise generator
        generator = netconf_generator("config.yaml")

        # get NetworkConfiguration
        network_configuration = next(generator)

        # print a dictionary describing the network configuration
        print(network_configuration.config())

        # get the actual objects that make up the network constructed using the builder(s) and linker(s)
        objects = network_configuration.objects()

    This function reads YAML configuration files and uses them to create `NetworkConfiguration` objects.
    The reason why this function returns a generator rather than a `NetworkConfiguration` object directly
    is that the generator allows for iterating over different network configurations that are defined through
    the use of special keywords in the YAML file. The special keywords that allow for this are `LIST` and `RANGE`.

    With `LIST`, one can specify a list of values to use in the configuration file.
    With `RANGE`, one can specify either a range of values with integer start, stop and step parameters,
    similar to the internal `range()` functionality of Python, while it also allows for a certain number
    of points between two values (can be floats), similar to NumPy's `linspace()`. Note that one can also influence
    which combinations of values are created through the `SYNC` keyword for `RANGE` parameters (more details are
    explained in the tutorial).

    This function also implements other special keywords that do not allow for iterating over multiple network
    configurations but can be used to write YAML configuration files more efficiently or in a more convenient way.
    These keywords are `INCLUDE` and `UPDATE`.
    The keyword `INCLUDE` is used to include parameters from other yaml files and `UPDATE` is used
    to enable updating YAML anchors partially such that anchors can be reused more economically.

    Note that there is also one special keyword (`PARAM`) that is not implemented by this function but rather
    implemented by the `NetworkConfiguration` class itself.

    For more information on how to use special keywords, see the tutorial section special keywords in the documentation
    of this snippet (`./docs/tutorial/special_keywords.rst`).
    For examples of configuration files, see `./examples` and `/tests.test_configs`.

    Parameters
    ----------
    config_file : string
        File name of the YAML configuration file.
    builders : list of :class:`~netsquid_netconf.builder.Builder` objects.
        Builders to be passed to the constructor of :class:`~netsquid_netconf.netconf.NetworkConfiguration`.
        These builders are used to construct the objects in the networks specified by the YAML file.
    linkers : list of Linker
        Linkers to be passed to the constructor of :class:`~netsquid_netconf.netconf.NetworkConfiguration`.
        These linkers are used to connect the objects in the networks specified by the YAML file.

    Returns
    -------
    generator : generator of :class:`~netsquid_netconf.netconf.NetworkConfiguration`.
        Generator that iterates over `NetworkConfiguration` objects as defined by the input YAML file.

    """

    # So that we can use !includes in the files
    Loader.add_constructor('!include', Loader.include)
    with open(config_file, 'r') as file_handle:
        config = yaml.load(file_handle, Loader) or {}

    # check which keys are not picked up by builders
    unpicked_builder_keys = list(set([builder.key() for builder in builders]) - set(config.keys()))
    unpicked_config_keys = list(set(config.keys()) - set([builder.key() for builder in builders]))
    if len(unpicked_builder_keys) > 0:
        logger.warning("The configuration file doesn't contain these builder keys: {}. "
                       "Check if that is the desired behaviour."
                       .format(unpicked_builder_keys))
    if len(unpicked_config_keys) > 0:
        logger.info("Builders didn't pick up these keys: {}. "
                    "Note that keys that define anchors are not picked up by builders and should appear here."
                    .format(unpicked_config_keys))

    # strip away anchors
    config = {key: config[key] for key in [builder.key() for builder in builders] if config.get(key, None) is not None}

    # evaluate all INCLUDE keywords
    for path in (keys for keys in flatten(config).keys() if len(keys) > 2 and "INCLUDE" in keys):
        # check that the component has not been updated yet
        if type(_nested_dict_get(config, path)) is not dict:  # getting a non-existing path returns a dict
            include_index = path.index("INCLUDE")
            # update with values from included external file (this should not overwrite already defined parameters)
            _nested_dict_update(nested_dict=config, update_path=path[:include_index],
                                update_dict=_nested_dict_get(config, path[:include_index + 1]), keyword="INCLUDE",
                                overwrite=False)

    # evaluate all UPDATE keywords
    for path in (keys for keys in flatten(config).keys() if len(keys) > 2 and "UPDATE" in keys):
        # check that the component has not been updated yet
        if type(_nested_dict_get(config, path)) is not dict:  # getting a non-existing path returns a dict
            update_index = path.index("UPDATE")
            # update with values from UPDATE keyword and overwrite values if the parameters are already defined
            _nested_dict_update(nested_dict=config, update_path=path[:update_index],
                                update_dict=_nested_dict_get(config, path[:update_index + 1]))

    # if there is a LIST or RANGE keyword, evaluate it
    if any(key[len(key) - 1] == "LIST" or key[len(key) - 2] == "RANGE" for key
           in flatten(config).keys()):
        for paths, values in _get_paths_and_values(config):
            # paths and values are now the values that should be set for one particular config
            new_config = config

            # set all the values that need to be replaced
            for path, value in zip(paths, values):
                _nested_dict_set(new_config, path, value)

            yield NetworkConfiguration(config=config, builders=builders, linkers=linkers)

    # if no LIST or RANGE, only one dictionary and configuration is yielded
    else:
        yield NetworkConfiguration(config=config, builders=builders, linkers=linkers)


def get_included_params(config_file):
    """
    Returns parameters that are specified in a separate parameter file, i.e. parameters from a file that is
    referred to using the INCLUDE keyword.

    Note that this method retrieves only the parameters specified by the first INCLUDE keyword.

    Parameters
    ----------
    config_file : string
        File name of the YAML configuration file containing imported parameters with keyword INCLUDE.

    Returns
    -------
    included : dict
        Dictionary with separately imported (included) parameters.
        If the configuration file does not contain INCLUDE keyword, empty dictionary is returned.
    """
    Loader.add_constructor('!include', Loader.include)
    with open(config_file, 'r') as file_handle:
        config = yaml.load(file_handle, Loader) or {}

    # get first included parameters
    for path in (keys for keys in flatten(config).keys() if len(keys) > 2 and "INCLUDE" in keys):
        included = _nested_dict_get(config, path[:path.index("INCLUDE") + 1])
        return included

    return {}


def _get_objects(config, builders, linkers):
    """Call all the Builders and Linkers and add them to output dictionary if any output is given.

    Parameters
    ----------
    config : dict
        Dictionary with all the configuration parameters, following the same structure as YAML config files.
        May not implement any of the special keywords.
    builders : list of :class:`~netsquid_netconf.builder.Builder` objects.
        Builders for the provided configuration.
    linkers : list of :class:`~netsquid_netconf.linker.Linker` objects.
        Linkers for the provided configuration.

    Returns
    -------
    objects : dict
        Dictionary where the key-value pairs are given by the keys of the builders and their respective output.

    """
    objects = {
        builder.key(): builder.build(config[builder.key()])
        for builder in filter(lambda b: b.key() in config, builders)
    }

    for linker in linkers:
        linker.link(objects, config)

    return objects


def _nested_dict_update(nested_dict, update_path, update_dict, keyword="UPDATE", overwrite=True):
    """
    Updates an arbitrarily nested dictionary.

    Parameters
    ----------
    nested_dict : dict
        Arbitrarily nested dictionary with some component that should be updated.
    update_path : list of strings
        Path to the component that should be updated.
    update_dict : dict
        Parameters the nested dictionary should be updated with.
    keyword : str
        Defines which keyword uses this method (which keyword should be popped from the dictionary).
        By default, this is "UPDATE". The other choice for this parameter is "INCLUDE".
    overwrite : bool
        Defines what should happen when a key from `update_dict` is already defined in `nested_dict`.
        By default, the value in `nested_dict` gets overwritten with the value from `update_dict`.

    """
    # deepcopy the component that needs to be updated
    component = deepcopy(_nested_dict_get(nested_dict, update_path))
    # update the component with the update dictionary
    _recursive_dict_update(component, update_dict, overwrite=overwrite)
    # remove the keyword from the updated component
    _nested_dict_pop(component, [keyword])
    # set the updated component back into the nested dictionary
    _nested_dict_set(nested_dict, update_path, component)
    return nested_dict


def _recursive_dict_update(to_be_updated, update, overwrite=True):
    """
    Recursively updates a component.

    Parameters
    ----------
    to_be_updated : dict
        The component (its parameters) that should be updated.
    update : dict
        Parameters the component should be updated with.
    overwrite : Boolean
        Defines what should happen when a key from `update` is already defined in `component`.
        By default, the value of `component` gets overwritten with the value of `update`.

    """
    for key, value in update.items():
        if isinstance(value, dict):
            if len(value.keys()) == 1 and ("RANGE" in value.keys() or "LIST" in value.keys()):  # dict is RANGE or LIST
                to_be_updated[key] = value
                return to_be_updated
            to_be_updated[key] = _recursive_dict_update(to_be_updated.get(key, {}), value)
        else:
            if to_be_updated.get(key, None) is None or overwrite:  # check if key should be overwritten
                to_be_updated[key] = value
    return to_be_updated


def _nested_dict_get(nested_dict, get_path):
    """
    Method for retrieving a value from arbitrarily nested dictionary.
    Note that this method returns an empty dictionary instead of raising KeyError if the path does not exist.

    Parameters
    ----------
    nested_dict : dict
        Arbitrarily nested dictionary with all configuration parameters.
    get_path : list of str
        Value from the dictionary with this path will be retrieved.

    Returns
    -------
    config : dict or some specific value (e.g. String, Int, Float)
        Dictionary or a specific value that is retrieved.

    """
    for key in get_path:
        nested_dict = nested_dict.get(key, {})
    return nested_dict


def _nested_dict_set(nested_dict, set_path, value):
    """Method for setting a value for a given path in an arbitrarily nested dictionary.

    Parameters
    ----------
    nested_dict : dict
        Dictionary with all the configuration parameters.
    set_path : list of str
        Path to the value in the dictionary that should be set.
    value : dict or some specific value (e.g. str, int, float)
        New value that should be set in the dictionary.

    """
    for key in set_path[:-1]:
        nested_dict = nested_dict.setdefault(key, {})
    nested_dict[set_path[-1]] = value


def _nested_dict_pop(nested_dict, pop_path):
    """
    Method for popping a value from arbitrarily nested dictionary.

    Parameters
    ----------
    nested_dict : dict
        Dictionary with all the configuration parameters.
    pop_path : list of str
        Value with this path in the dictionary will be popped.

    """
    for key in pop_path[:-1]:
        nested_dict = nested_dict[key]
    nested_dict.pop(pop_path[-1])


def _get_paths_and_values(config):
    """
    Extract values and their positions that should be replaced when config contains RANGE or LIST.

    Parameters
    ----------
    config : dict
        Dictionary with all the configuration parameters.

    Yields
    ------
    paths : list of list of String
        List of paths to occurrences of RANGE or LIST keywords.
    values : list of values (e.g. String, Int, Float)
        List of values that need to be set when replacing RANGE or LIST keywords.

    """
    flat_components = flatten(config)

    sync_keywords = []

    paths = []
    values = []
    sync_paths = []
    sync_values = []

    # add all the paths and values for LIST keyword
    for path in (keys for keys in flat_components.keys() if keys[len(keys) - 1] == "LIST"):
        paths.append(list(path[:-1]))
        values.append(flat_components.get(path, None))

    # add all the paths and values for RANGE keyword
    for path in (keys for keys in flat_components.keys() if keys[len(keys) - 2] == "RANGE"):
        # make sure this path isn't already in the list
        if not any(list(path[:-2]) == existing_path for existing_path in (paths + sync_paths)):
            # get the nested RANGE dict
            range_values = _nested_dict_get(config, list(path[:-1]))

            # check RANGE has correct parameters
            if len(range_values) != 3 and len(range_values) != 4:
                raise ValueError("Range wrong number of arguments")
            if range_values.get("START", None) is None \
                    or range_values.get("STOP", None) is None:
                raise ValueError("Missing boundaries for range exploration")
            if (range_values.get("STEP", None) is None) is (range_values.get("NUM", None) is None):
                raise ValueError("Either both or none of range and linear space exploration is defined")
            if range_values.get("NUM", None) is None and range_values.get("ENDPOINT", None) is not None:
                raise ValueError("Cannot define whether to exclude endpoint when specifying step size")

            # define where to add the path and range values depending on whether there is SYNC keyword
            if range_values.get("SYNC", None) is None:
                paths_list, values_list = paths, values
            else:
                sync_keywords.append(range_values.get("SYNC"))
                paths_list, values_list = sync_paths, sync_values

            paths_list.append(list(path[:-2]))

            # append the range generator to values
            if range_values.get("STEP", None) is not None:
                values_list.append(range(range_values["START"], range_values["STOP"], range_values["STEP"]))
            else:
                values_list.append(linspace(range_values["START"], range_values["STOP"],
                                            num=range_values["NUM"], endpoint=range_values.get("ENDPOINT", True)))

    # raise error if SYNC keywords vary
    if len(set(sync_keywords)) > 1:
        raise NotImplementedError("SYNC keywords across the whole YAML config file should be the same")

    # raise error if SYNC is used on RANGE values with different length
    if not all(len(el) == len(sync_values[0]) for el in sync_values):
        raise ValueError("SYNC should be defined for RANGE with equal number of possible values")

    # if there was only one keyword in total, wrap the values in a list
    if len(values) + len(sync_values) == 1:
        values += sync_values
        combinations = map(lambda x: [x], values[0])
    else:
        # if there was only one SYNC keyword, it can be simply combined with all values
        if len(sync_values) == 1:
            values += sync_values
        # zip any SYNC values
        if len(sync_values) > 1:
            sync_values = list(zip(*sync_values))
            values.append(sync_values)
        # get all the possible combinations
        combinations = product(*values)

    # yield for every possible configuration
    for comb in combinations:
        if len(sync_values) > 1:  # values might contain nested lists
            comb = list(chain(comb[:-1], comb[-1]))
        yield paths + sync_paths, comb
