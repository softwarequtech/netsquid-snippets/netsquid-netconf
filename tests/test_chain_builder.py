from netsquid_netconf.builder import ComponentBuilder
from netsquid_netconf.netconf import netconf_generator

from netsquid_netconf.chain_builder import NodeCoordinateCalculator, ChainBuilder
import numpy as np
import unittest
import os

from netsquid.nodes.connections import Connection
from netsquid.nodes.node import Node


class TestNode(Node):
    def __init__(self, name, end_node, num_positions, port_names=None):
        super().__init__(name=name, port_names=port_names)
        self.end_node = end_node
        self.num_pos = num_positions


class AsymConnection(Connection):
    def __init__(self, name, length_A, length_B):
        super().__init__(name=name)
        self.length_A = length_A
        self.length_B = length_B
        self.length = length_A + length_B


class SymConnection(Connection):
    def __init__(self, name, length):
        super().__init__(name=name)
        self.length = length


class TestNodeCoordinateCalculator(unittest.TestCase):
    """Test :class:`NodeCoordinateCalculator`."""

    def setUp(self) -> None:
        self.total_length = 100
        self.num_nodes = 11

    def test_symmetric(self):
        """Test a symmetric, regular chain is obtained when asymmetry parameter is zero."""
        asymmetry_parameter = 0
        node_coordinate_calculator = NodeCoordinateCalculator(total_length=self.total_length,
                                                              num_nodes=self.num_nodes,
                                                              asymmetry_parameter=asymmetry_parameter)
        node_coordinate_calculator.signs = [+1] * (self.num_nodes - 2)
        coordinates = node_coordinate_calculator.coordinates
        num_segments = self.num_nodes - 1
        for i in range(len(coordinates) - 1):
            assert coordinates[i + 1] == coordinates[i] + self.total_length / num_segments

    def test_two_to_one(self):
        """Assert that when asymmetry parameter = 1 / 3, the ratio between segments is 2 : 1."""
        asymmetry_parameter = 1 / 3
        node_coordinate_calculator = NodeCoordinateCalculator(total_length=self.total_length,
                                                              num_nodes=self.num_nodes,
                                                              asymmetry_parameter=asymmetry_parameter)
        node_coordinate_calculator.signs = [+1] * (self.num_nodes - 2)
        coordinates = node_coordinate_calculator.coordinates
        for i in range(len(coordinates) - 2):
            assert np.isclose(2 * (coordinates[i + 2] - coordinates[i + 1]),
                              coordinates[i + 1] - coordinates[i])

    def test_large_asymmetry(self):
        """Check that if asymmetry is large and alternating, groups of two close-together nodes are formed."""
        asymmetry_parameter = 1 - 1E-2
        node_coordinate_calculator = NodeCoordinateCalculator(total_length=self.total_length,
                                                              num_nodes=self.num_nodes,
                                                              asymmetry_parameter=asymmetry_parameter)
        node_coordinate_calculator.set_signs_alternating()
        coordinates = node_coordinate_calculator.coordinates
        num_segments = self.num_nodes - 1
        for i in range(len(coordinates) - 1):
            # Nodes are alternatingly far apart and close together.
            # This results in effectively only half the number of total segments, each of equal length.
            # Between all segments are two nodes lying close together.
            if i % 2 == 1:
                assert np.isclose(coordinates[i + 1], coordinates[i], atol=0.2)
            else:
                assert np.isclose(coordinates[i+1] - coordinates[i], 2 * self.total_length / num_segments, atol=0.2)


def test_chain_builder_with_midpoints():
    component_builder = ComponentBuilder()
    component_builder.add_type(name="test_node", new_type=TestNode)
    component_builder.add_type(name="asym_connection", new_type=AsymConnection)
    generator = netconf_generator("tests/test_configs/chain_builder_with_midpoints.yaml",
                                  builders=[ChainBuilder(component_builder)], linkers=[])
    network_configuration = next(generator)
    objects = network_configuration.objects()
    network = objects["chain"]["network"]
    assert len(network.nodes) == 6  # midpoints are not netsquid nodes
    for i in range(6):
        is_end_node = network.nodes[f"node_{i}"].end_node
        num_pos = network.nodes[f"node_{i}"].num_pos
        if i == 0 or i == 5:
            assert is_end_node
            assert num_pos == 1
        else:
            assert not is_end_node
            assert num_pos == 2

    for connection in network.connections.values():
        assert isinstance(connection, AsymConnection)
        len_A = connection.length_A
        len_B = connection.length_B
        assert np.isclose(len_A, 2 * len_B)

    assert os.path.exists("with_midpoints.png")
    os.remove("with_midpoints.png")


def test_chain_builder_without_midpoints():
    component_builder = ComponentBuilder()
    component_builder.add_type(name="test_node", new_type=TestNode)
    component_builder.add_type(name="sym_connection", new_type=SymConnection)
    generator = netconf_generator("tests/test_configs/chain_builder_without_midpoints.yaml",
                                  builders=[ChainBuilder(component_builder)], linkers=[])
    network_configuration = next(generator)
    objects = network_configuration.objects()
    network = objects["chain"]["network"]
    assert len(network.nodes) == 11
    for conn in network.connections.values():
        assert isinstance(conn, SymConnection)
    for i in range(9):
        node1 = network.nodes[f"node_{i}"]
        if i == 0:
            assert node1.end_node
            assert node1.num_pos == 1
        else:
            assert not node1.end_node
            assert node1.num_pos == 2
        node2 = network.nodes[f"node_{i + 1}"]
        assert not node2.end_node
        assert node2.num_pos == 2
        node3 = network.nodes[f"node_{i + 2}"]
        if i == 8:
            assert node3.end_node
            assert node3.num_pos == 1
        else:
            assert not node3.end_node
            assert node3.num_pos == 2
        conn1_classical = network.get_connection(node1, node2, label="classical_connection")
        conn2_classical = network.get_connection(node2, node3, label="classical_connection")
        assert np.isclose(conn1_classical.length, 2 * conn2_classical.length)
        conn1_entangled = network.get_connection(node1, node2, label="entanglement_connection")
        conn2_entangled = network.get_connection(node2, node3, label="entanglement_connection")
        assert np.isclose(conn1_entangled.length, 2 * conn2_entangled.length)

    assert os.path.exists("no_midpoints.png")
    os.remove("no_midpoints.png")


def test_chain_builder_with_asym_midpoints():
    component_builder = ComponentBuilder()
    component_builder.add_type(name="test_node", new_type=TestNode)
    component_builder.add_type(name="asym_connection", new_type=AsymConnection)
    generator = netconf_generator("tests/test_configs/chain_builder_with_asym_midpoints.yaml",
                                  builders=[ChainBuilder(component_builder)], linkers=[])
    network_configuration = next(generator)
    objects = network_configuration.objects()
    network = objects["chain"]["network"]
    assert len(network.nodes) == 6
    for i in range(6):
        is_end_node = network.nodes[f"node_{i}"].end_node
        num_pos = network.nodes[f"node_{i}"].num_pos
        if i == 0 or i == 5:
            assert is_end_node
            assert num_pos == 1
        else:
            assert not is_end_node
            assert num_pos == 2
    for connection in network.connections.values():
        assert isinstance(connection, AsymConnection)
        assert connection.length == 20  # every connection should have same length since asymmetry = 0
        len_A = connection.length_A
        len_B = connection.length_B
        assert np.isclose(len_A, 2 * len_B)  # midpoints should still be displaced though

    assert os.path.exists("asym_midpoints.png")
    os.remove("asym_midpoints.png")
