# This file uses NumPy style docstrings:
# https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt

"""Tests for the NetConf snippet."""

from netsquid.components.cchannel import ClassicalChannel
from netsquid.components.qmemory import QuantumMemory
from netsquid.components.models.delaymodels import FibreDelayModel
from netsquid.nodes.connections import DirectConnection, Connection
from netsquid.nodes.node import Node

from netsquid_netconf.builder import ComponentBuilder, NetworkBuilder
from netsquid_netconf.linker import ComponentLinker, NetworkLinker
from netsquid_netconf.netconf import netconf_generator, get_included_params, Loader, _nested_dict_set

from numpy import linspace
from itertools import product

import os
import unittest
import yaml


class ClassicalFibreDirectConnection(DirectConnection):
    """A direct connection composed of symmetric classical fibres."""

    def __init__(self, name, **properties):
        super().__init__(
            name=name,
            channel_AtoB=ClassicalChannel(
                name="AtoB",
                models={"delay_model": FibreDelayModel()},
                **properties,
            ),
            channel_BtoA=ClassicalChannel(
                name="BtoA",
                models={"delay_model": FibreDelayModel()},
                **properties,
            ),
        )


class TestNetconf(unittest.TestCase):

    def setUp(self):
        self.component_builder = ComponentBuilder()
        self.component_builder.add_type(
            "classical_fibre_direct_connection",
            ClassicalFibreDirectConnection,
        )
        self.component_builder.add_type(
            "quantum_memory", QuantumMemory
        )

    def test_empty(self):
        """Test basic loading and initialisation by loading an empty file."""
        generator = netconf_generator("./tests/test_configs/empty.yaml",
                                      builders=[self.component_builder, NetworkBuilder()], linkers=[])
        network_configuration = next(generator)
        builder_outputs = network_configuration.objects()
        config = network_configuration.config()

        self.assertEqual(len(builder_outputs), 0)
        self.assertEqual(len(config), 0)
        self.assertIsNone(builder_outputs.get("components", None))
        self.assertIsNone(builder_outputs.get("network", None))

    def test_component_builder_and_component_linker(self):
        """Test loading a simple non-empty configuration file using both the ComponentBuilder and ComponentLinker."""
        generator = netconf_generator("./tests/test_configs/config.yaml", builders=[self.component_builder],
                                      linkers=[ComponentLinker()])
        network_configuration = next(generator)
        builder_outputs = network_configuration.objects()

        self.assertIsNotNone(builder_outputs.get("components", None))
        self.assertIsNone(builder_outputs.get("network", None))

        self.assertIsInstance(builder_outputs["components"]["Node0"], Node)
        self.assertIsInstance(builder_outputs["components"]["Node1"], Node)
        self.assertIsInstance(builder_outputs["components"]["Connection0"], ClassicalFibreDirectConnection)

        self.assertFalse(builder_outputs["components"]["Node0"].ports["A"].is_connected)
        self.assertTrue(builder_outputs["components"]["Node0"].ports["B"].is_connected)

        node_0 = builder_outputs["components"]["Node0"]
        node_1 = builder_outputs["components"]["Node1"]
        node_2 = builder_outputs["components"]["Node2"]
        nodes = [node_0, node_1, node_2]
        connection = builder_outputs["components"]["Connection0"]

        [self.assertIsInstance(node, Node) for node in nodes]
        self.assertIsInstance(connection, ClassicalFibreDirectConnection)

        # Check if nodes contain a quantum memory with the correct property
        for node in nodes:
            self.assertIsNotNone(node.subcomponents.get("quantum_memory"))
            self.assertEqual(node.subcomponents["quantum_memory"].num_positions, 10)

        # Check if nodes are connected correctly
        self.assertFalse(node_0.ports["A"].is_connected)
        self.assertTrue(node_0.ports["B"].is_connected)

        self.assertTrue(node_1.ports["A"].is_connected)
        self.assertTrue(node_1.ports["B"].is_connected)

        self.assertTrue(node_2.ports["A"].is_connected)
        self.assertFalse(node_2.ports["B"].is_connected)

        self.assertTrue(connection.ports["A"].is_connected)
        self.assertTrue(connection.ports["B"].is_connected)

    def test_node_config_update_connection(self):
        """
        Test that anchors can be used for baseline parameters and specific values can be updated for each component.

        See `tests/test_configs/update.yaml` for how this is done.

        This tests updating the properties of a connection component (i.e. built in ComponentBuilder).
        """
        generator = netconf_generator("./tests/test_configs/update.yaml", builders=[self.component_builder],
                                      linkers=[ComponentLinker()])
        network_configuration = next(generator)
        builder_outputs = network_configuration.objects()
        config = network_configuration.config()

        # check network component
        self.assertIsNotNone(builder_outputs.get("components", None))
        self.assertIsNone(builder_outputs.get("network", None))

        self.assertIsInstance(builder_outputs["components"]["NodeA"], Node)
        self.assertIsInstance(builder_outputs["components"]["NodeB"], Node)
        self.assertIsInstance(builder_outputs["components"]["connection"], ClassicalFibreDirectConnection)

        connection_properties = config.get("components", None).get("connection", None).get("properties", None)
        self.assertEqual(connection_properties.get("length", None), 1)
        self.assertEqual(connection_properties.get("p_loss_init", None), 0)
        self.assertEqual(connection_properties.get("p_loss_length", None), 0)

    def test_node_config_update_connection_with_range_or_list(self):
        """
        Test that the `UPDATE` keyword works with `RANGE` and `LIST` keywords.

        This tests updating the properties of a connection component (i.e. built in ComponentBuilder)
        """
        basis_config = yaml.load(open("./tests/test_configs/update.yaml", 'r'), Loader) or {}
        basis_config = {"components": basis_config["components"]}

        path = ("components", "connection", "UPDATE", "properties", "length")

        range = {"RANGE": {"START": 1, "STOP": 4, "STEP": 1}}
        list = {"LIST": [1, 2, 3]}

        for keyword in [range, list]:
            updated_config = dict(basis_config)
            _nested_dict_set(updated_config, path, keyword)

            yaml.dump(updated_config, open("./tests/test_configs/temp.yaml", "w"))
            generator = netconf_generator("./tests/test_configs/temp.yaml",
                                          builders=[self.component_builder], linkers=[ComponentLinker()])

            for i in [1, 2, 3]:
                network_configuration = next(generator)
                builder_outputs = network_configuration.objects()
                output_config = network_configuration.config()

                # check network component
                self.assertIsNotNone(builder_outputs.get("components", None))
                self.assertIsNone(builder_outputs.get("network", None))

                # check length of created connection
                connection_properties = output_config.get("components", None).get("connection", None)\
                    .get("properties", None)
                self.assertEqual(connection_properties.get("length", None), i)

            os.remove("./tests/test_configs/temp.yaml")

    def test_node_config_update_network(self):
        """
        Test that anchors can be used for baseline parameters and specific values can be updated for properties needed
        in NetworkBuilder.

        See `tests/test_configs/update_problem.yaml` for how this is done.

        This tests updating the properties of a connection within a network (i.e. built in NetworkBuilder).
        """
        component_builder = ComponentBuilder()
        component_builder.add_type(name="Connection", new_type=Connection)
        generator = netconf_generator("./tests/test_configs/update_problem.yaml",
                                      builders=[component_builder, NetworkBuilder()], linkers=[NetworkLinker()])
        network_configuration = next(generator)
        builder_outputs = network_configuration.objects()

        self.assertEqual(len(builder_outputs), 2)

        network = builder_outputs.get("network", None)
        self.assertIsNotNone(network)

        self.assertEqual(len(network.nodes), 4)
        self.assertEqual(len(network.connections), 2)
        self.assertEqual(network.name, "test_network")

        node_a, node_b, node_c, node_d = network.get_node("NodeA"), network.get_node("NodeB"), \
            network.get_node("NodeC"), network.get_node("NodeD")
        self.assertIsNotNone(node_a)
        self.assertIsNotNone(node_b)
        self.assertIsNotNone(node_c)
        self.assertIsNotNone(node_d)

        self.assertFalse(node_a.ports["top"].is_connected)
        self.assertFalse(node_a.ports["bottom"].is_connected)

        connection_top = network.get_connection(node_b, node_c, label="top")
        connection_bottom = network.get_connection(node_c, node_d, label="bottom")
        self.assertIsNotNone(connection_top)
        self.assertIsNotNone(connection_bottom)

    def test_node_config_list(self):
        """Test LIST functionality in configuration file."""
        generator = netconf_generator("./tests/test_configs/list.yaml", builders=[self.component_builder], linkers=[])

        for i in [2, 4, 6, 8, 10]:
            network_configuration = next(generator)
            builder_outputs = network_configuration.objects()
            config = network_configuration.config()

            # check network component
            self.assertIsNotNone(builder_outputs.get("components", None))
            self.assertIsNone(builder_outputs.get("network", None))

            # check length of created connection
            connection_properties = config.get("components", None).get("connection", None).get("properties", None)
            self.assertEqual(connection_properties.get("length", None), i)

    def test_node_config_range(self):
        """Test RANGE functionality in configuration file."""
        generator = netconf_generator("./tests/test_configs/range.yaml",
                                      builders=[self.component_builder], linkers=[ComponentLinker()])

        for i in range(10):
            network_configuration = next(generator)
            builder_outputs = network_configuration.objects()
            config = network_configuration.config()

            # check network component
            self.assertIsNotNone(builder_outputs.get("components", None))
            self.assertIsNone(builder_outputs.get("network", None))

            # check length of created connection
            connection_properties = config.get("components", None).get("connection", None).get("properties", None)
            self.assertEqual(connection_properties.get("length", None), i)

    def test_node_config_linspace(self):
        """Test LINSPACE functionality in configuration file."""
        generator = netconf_generator("./tests/test_configs/linspace.yaml",
                                      builders=[self.component_builder], linkers=[ComponentLinker()])

        for i in linspace(0, 1, 10, False):
            network_configuration = next(generator)
            builder_outputs = network_configuration.objects()
            config = network_configuration.config()

            # check network component
            self.assertIsNotNone(builder_outputs.get("components", None))
            self.assertIsNone(builder_outputs.get("network", None))

            # check length of created connection
            connection_properties = config.get("components", None).get("connection", None).get("properties", None)
            self.assertEqual(connection_properties.get("p_loss_init", None), i)

    def test_node_config_two_dimensions(self):
        """Test iterating over multiple dimensions in configuration file."""
        generator = netconf_generator("./tests/test_configs/two_dimensions.yaml",
                                      builders=[self.component_builder], linkers=[ComponentLinker()])

        for length in [1, 2, 3]:
            for loss in range(4, 7):
                network_configuration = next(generator)
                builder_outputs = network_configuration.objects()
                config = network_configuration.config()

                # check network component
                self.assertIsNotNone(builder_outputs.get("components", None))
                self.assertIsNone(builder_outputs.get("network", None))

                # check length of created connection
                connection_properties = config.get("components", None).get("connection", None).get("properties", None)
                self.assertEqual(connection_properties.get("length", None), length)
                self.assertEqual(connection_properties.get("p_loss_init", None), loss)

    def test_node_config_wrong_params(self):
        """Test RANGE functionality with wrong parameters."""
        basis_config = yaml.load(open("./tests/test_configs/range.yaml", 'r'), Loader) or {}
        basis_config = {"components": basis_config["components"]}

        path = ("components", "connection", "properties", "length", "RANGE")

        test1 = {"START": 0, "STOP": 2}
        test2 = {"STOP": 10, "NUM": 10, "ENDPOINT": True}
        test3 = {"START": 1, "NUM": 10, "ENDPOINT": True}
        test4 = {"START": 1, "STOP": 10, "ENDPOINT": True}
        test5 = {"START": 1, "STOP": 10, "STEP": 1, "NUM": 10}
        test6 = {"START": 1, "STOP": 10, "STEP": 1, "ENDPOINT": True}

        for test in [test1, test2, test3, test4, test5, test6]:
            updated_config = dict(basis_config)
            _nested_dict_set(updated_config, path, test)

            yaml.dump(updated_config, open("./tests/test_configs/wrong.yaml", "w"))
            generator = netconf_generator("./tests/test_configs/wrong.yaml",
                                          builders=[self.component_builder], linkers=[ComponentLinker()])

            with self.assertRaises(ValueError):
                next(generator)

            os.remove("./tests/test_configs/wrong.yaml")

    def test_network_builder(self):
        """Test the NetworkBuilder using an example and check the resulting Network."""
        generator = netconf_generator("./tests/test_configs/network.yaml",
                                      builders=[self.component_builder, NetworkBuilder()],
                                      linkers=[NetworkLinker()])
        network_configuration = next(generator)
        builder_outputs = network_configuration.objects()

        self.assertEqual(len(builder_outputs), 2)

        network = builder_outputs.get("network", None)
        self.assertIsNotNone(network)
        self.assertEqual(len(network.nodes), 2)
        self.assertEqual(len(network.connections), 2)
        self.assertEqual(network.name, "test_network")

        node_a, node_b = network.get_node("NodeA"), network.get_node("NodeB")
        self.assertIsNotNone(node_a)
        self.assertIsNotNone(node_b)

        for node in [node_a, node_b]:
            self.assertTrue(node.ports["top"].is_connected)
            self.assertTrue(node.ports["bottom"].is_connected)

        connection_top = network.get_connection(node_a, node_b, label="top")
        connection_bottom = network.get_connection(node_a, node_b, label="bottom")
        self.assertIsNotNone(connection_top)
        self.assertIsNotNone(connection_bottom)

        for connection in [connection_top, connection_bottom]:
            self.assertTrue(connection.ports["A"].is_connected)
            self.assertTrue(connection.ports["B"].is_connected)

    def test_no_connections_in_network(self):
        """Test that Network with Nodes that are not connected is returned if no `connect_to` is defined."""
        config = yaml.load(open("./tests/test_configs/config.yaml", 'r'), Loader) or {}
        config.update({"network": "test_network"})

        yaml.dump(config, open("./tests/test_configs/temp.yaml", "w"))
        generator = netconf_generator("./tests/test_configs/temp.yaml",
                                      builders=[self.component_builder, NetworkBuilder()],
                                      linkers=[NetworkLinker()])

        network_configuration = next(generator)
        builder_outputs = network_configuration.objects()
        network = builder_outputs.get("network", None)

        self.assertIsNotNone(network)
        self.assertEqual(network.name, "test_network")
        self.assertEqual(len(network.nodes), 3)
        self.assertEqual(len(network.connections), 0)

        os.remove("./tests/test_configs/temp.yaml")

    def test_empty_network(self):
        """Test that Network is empty if its name is defined but no components are defined."""
        network_name_defined = {"network": "test_network"}
        yaml.dump(network_name_defined, open("./tests/test_configs/wrong.yaml", "w"))
        generator = netconf_generator("./tests/test_configs/wrong.yaml",
                                      builders=[self.component_builder, NetworkBuilder()],
                                      linkers=[NetworkLinker()])

        network_configuration = next(generator)
        builder_outputs = network_configuration.objects()
        network = builder_outputs.get("network", None)

        self.assertIsNotNone(network)
        self.assertEqual(network.name, "test_network")
        self.assertEqual(len(network.nodes), 0)
        self.assertEqual(len(network.connections), 0)

        os.remove("./tests/test_configs/wrong.yaml")

    def test_range_sync(self):
        """Test that SYNC works correctly when there are also some components without SYNC."""
        generator = netconf_generator("./tests/test_configs/sync.yaml",
                                      builders=[self.component_builder], linkers=[ComponentLinker()])

        values = [[10, 10, 10], [20, 10, 20], [30, 10, 30], [10, 20, 10], [20, 20, 20], [30, 20, 30],
                  [10, 30, 10], [20, 30, 20], [30, 30, 30]]

        for i, network_configuration in enumerate(generator):
            # check length of created connection
            config = network_configuration.config()
            lenL = config.get("components", None).get("connectionL", None).get("properties", None).get("length", None)
            lenM = config.get("components", None).get("connectionM", None).get("properties", None).get("length", None)
            lenR = config.get("components", None).get("connectionR", None).get("properties", None).get("length", None)

            expected = values[i]

            self.assertEqual(lenL, expected[0])
            self.assertEqual(lenM, expected[1])
            self.assertEqual(lenR, expected[2])

    def test_range_sync_all_components(self):
        """Test that SYNC keyword produces correct combinations if all RANGE keywords are annotated with it.

        Network for this test:
         -------                     -------                     -------                     -------
        | NodeA |   -------------   | NodeB |   -------------   | NodeC |   -------------   | NodeD |
        |B     A|<>| ConnectionL |<>|B     A|<>| ConnectionM |<>|B     A|<>| ConnectionR |<>|B     A|
        |       |   -------------   |       |   -------------   |       |   -------------   |       |
         -------                     -------                     -------                     -------
                        SYNC                        SYNC                        SYNC

        """
        with open("./tests/test_configs/sync.yaml", 'r') as f:
            basis_config = yaml.load(f, Loader) or {}
        basis_config["components"]["connectionM"]["UPDATE"]["properties"] = \
            {"length": {"RANGE": {"START": 10, "STOP": 40, "STEP": 10, "SYNC": "values"}}}
        with open("./tests/test_configs/temp.yaml", "w") as f:
            yaml.dump(basis_config, f)

        generator = netconf_generator("./tests/test_configs/temp.yaml",
                                      builders=[self.component_builder], linkers=[ComponentLinker()])

        values = [[10, 10, 10], [20, 20, 20], [30, 30, 30]]

        for i, network_configuration in enumerate(generator):
            # check length of created connection
            output_config = network_configuration.config()
            lenL = output_config.get("components", None).get("connectionL", None).get("properties", None)\
                .get("length", None)
            lenM = output_config.get("components", None).get("connectionM", None).get("properties", None)\
                .get("length", None)
            lenR = output_config.get("components", None).get("connectionR", None).get("properties", None)\
                .get("length", None)

            expected = values[i]

            self.assertEqual(lenL, expected[0])
            self.assertEqual(lenM, expected[1])
            self.assertEqual(lenR, expected[2])

        os.remove("./tests/test_configs/temp.yaml")

    def test_range_sync_single_component(self):
        """Test that SYNC keyword has no effect if there is only one component annotated with it.

        Network for this test:
         -------                     -------                     -------                     -------
        | NodeA |   -------------   | NodeB |   -------------   | NodeC |   -------------   | NodeD |
        |B     A|<>| ConnectionL |<>|B     A|<>| ConnectionM |<>|B     A|<>| ConnectionR |<>|B     A|
        |       |   -------------   |       |   -------------   |       |   -------------   |       |
         -------                     -------                     -------                     -------
                        SYNC

        """
        with open("./tests/test_configs/sync.yaml", 'r') as f:
            basis_config = yaml.load(f, Loader) or {}
        del basis_config["components"]["connectionR"]["UPDATE"]["properties"]
        with open("./tests/test_configs/temp.yaml", "w") as f:
            yaml.dump(basis_config, f)

        generator = netconf_generator("./tests/test_configs/temp.yaml",
                                      builders=[self.component_builder], linkers=[ComponentLinker()])

        values = list(product((10, 20, 30), (10, 20, 30), (10, 20, 30)))

        for network_configuration in generator:
            # check length of created connection
            output_config = network_configuration.config()
            lenL = output_config.get("components", None).get("connectionL", None).get("properties", None)\
                .get("length", None)
            lenM = output_config.get("components", None).get("connectionM", None).get("properties", None)\
                .get("length", None)
            lenR = output_config.get("components", None).get("connectionR", None).get("properties", None)\
                .get("length", None)

            lengths = (lenL, lenM, lenR)
            self.assertTrue(lengths in values)
            values.remove(lengths)

        self.assertEqual(len(values), 0)

        os.remove("./tests/test_configs/temp.yaml")

    def test_range_sync_longer_chain(self):
        """Test that SYNC works correctly on a longer chain (multiple RANGEs with and without SYNC)."""
        generator = netconf_generator("./tests/test_configs/sync_longer_chain.yaml",
                                      builders=[self.component_builder], linkers=[ComponentLinker()])

        values = [x for x in list(product((10, 20, 30), (10, 20, 30), (10, 20, 30), (10, 20, 30))) if x[0] == x[3]]

        for network_configuration in generator:
            config = network_configuration.config()
            lenL = config.get("components", None).get("connectionL", None).get("properties", None).get("length", None)
            lenM0 = config.get("components", None).get("connectionM0", None).get("properties", None).get("length", None)
            lenM1 = config.get("components", None).get("connectionM1", None).get("properties", None).get("length", None)
            lenR = config.get("components", None).get("connectionR", None).get("properties", None).get("length", None)

            lengths = (lenL, lenM0, lenM1, lenR)
            self.assertTrue(lengths in values)
            values.remove(lengths)

        self.assertEqual(len(values), 0)

    def test_range_sync_varying_keywords(self):
        """Test that NotImplementedError is raised if SYNC has multiple keywords in one config file.

        Network for this test:
         -------                     -------                     -------                     -------
        | NodeA |   -------------   | NodeB |   -------------   | NodeC |   -------------   | NodeD |
        |B     A|<>| ConnectionL |<>|B     A|<>| ConnectionM |<>|B     A|<>| ConnectionR |<>|B     A|
        |       |   -------------   |       |   -------------   |       |   -------------   |       |
         -------                     -------                     -------                     -------
                    SYNC: "other"                                          SYNC: "values"

        """
        with open("./tests/test_configs/sync.yaml", 'r') as f:
            config = yaml.load(f, Loader) or {}
        config["components"]["connectionL"]["UPDATE"]["properties"]["length"]["RANGE"]["SYNC"] = "different"
        with open("./tests/test_configs/temp.yaml", "w") as f:
            yaml.dump(config, f)

        generator = netconf_generator("./tests/test_configs/temp.yaml",
                                      builders=[self.component_builder], linkers=[ComponentLinker()])

        with self.assertRaises(NotImplementedError):
            _, _ = next(generator)

        os.remove("./tests/test_configs/temp.yaml")

    def test_range_sync_varying_lengths(self):
        """Test that ValueError is raised if there is SYNC for RANGEs with different lengths."""
        with open("./tests/test_configs/sync.yaml", 'r') as f:
            config = yaml.load(f, Loader) or {}
        config["components"]["connectionL"]["UPDATE"]["properties"]["length"]["RANGE"]["STOP"] = 50
        with open("./tests/test_configs/temp.yaml", "w") as f:
            yaml.dump(config, f)

        generator = netconf_generator("./tests/test_configs/temp.yaml",
                                      builders=[self.component_builder], linkers=[ComponentLinker()])

        with self.assertRaises(ValueError):
            _, _ = next(generator)

        os.remove("./tests/test_configs/temp.yaml")

    def test_include(self):
        """Test that INCLUDE keyword works."""
        # check that INCLUDE appends parameters defined in external file but does not overwrite any defined parameters
        generator = netconf_generator("./tests/test_configs/include.yaml",
                                      builders=[self.component_builder], linkers=[ComponentLinker()])
        network_configuration = next(generator)
        config = network_configuration.config()

        fibre_properties = config["components"]["connection"]["properties"]
        # length is defined only in include.yaml
        self.assertEqual(1, fibre_properties["length"])
        # p_loss_init is defined in include.yaml and is not overwritten by a value from fibre_params.yaml
        self.assertEqual(0, fibre_properties["p_loss_init"])
        # p_loss_length gets added to the parameters as it is only defined in fibre_params.yaml
        self.assertEqual(0, fibre_properties["p_loss_length"])

    def test_get_included_parameters(self):
        """Test that the correct parameters are returned upon calling `get_included_parameters`."""
        imported = get_included_params("./tests/test_configs/include.yaml")
        with open("./tests/test_configs/fibre_params.yaml", 'r') as f:
            params = yaml.load(f, Loader) or {}

        self.assertDictEqual(imported, params)

    def test_get_included_parameters_empty(self):
        """Test that empty dictionary is returned when the config file does not contain the INCLUDE keyword."""
        imported = get_included_params("./tests/test_configs/network.yaml")
        empty = {}

        self.assertDictEqual(imported, empty)

    def test_param(self):
        """Test that the PARAM keyword is correctly implemented."""
        generator = netconf_generator("./tests/test_configs/param.yaml",
                                      builders=[self.component_builder], linkers=[ComponentLinker()])
        network_configuration = next(generator)
        assert (network_configuration.raw_config["components"]["connectionBC"]["properties"]["length"]
                == {"PARAM": "length_1"})

        # test error is raised when using wrong parameters
        assert network_configuration.parameters == {"length_1", "length_2"}
        with self.assertRaises(ValueError):
            network_configuration.config(random_parameter=100)
        with self.assertRaises(ValueError):
            network_configuration.objects(random_parameter=100)
        with self.assertRaises(ValueError):
            network_configuration.config(length_1=100)
        with self.assertRaises(ValueError):
            network_configuration.objects(length_1=100)
        with self.assertRaises(ValueError):
            network_configuration.config()
        with self.assertRaises(ValueError):
            network_configuration.objects()

        # test that things work as expected when parameters are correctly filled in
        config = network_configuration.config(length_1=100, length_2=200)
        assert config == network_configuration.config(length_1=100, length_2=200)  # check if it works repeatedly
        self.assertEqual(100, config["components"]["connectionAB"]["properties"]["length"])
        self.assertEqual(100, config["components"]["connectionBC"]["properties"]["length"])
        self.assertEqual(200, config["components"]["connectionCD"]["properties"]["length"])
        self.assertEqual(0., config["components"]["connectionAB"]["properties"]["p_loss_init"])
        objects = network_configuration.objects(length_1=100, length_2=200)
        assert objects["components"]["connectionAB"].subcomponents["channel_AtoB"].properties["length"] == 100
        assert objects["components"]["connectionBC"].subcomponents["channel_AtoB"].properties["length"] == 100
        assert objects["components"]["connectionCD"].subcomponents["channel_AtoB"].properties["length"] == 200

        # check that parameters can be changed without "memory effects"
        config = network_configuration.config(length_1=6, length_2=50)
        self.assertEqual(6, config["components"]["connectionAB"]["properties"]["length"])
        self.assertEqual(6, config["components"]["connectionBC"]["properties"]["length"])
        self.assertEqual(50, config["components"]["connectionCD"]["properties"]["length"])
        self.assertEqual(0., config["components"]["connectionAB"]["properties"]["p_loss_init"])
        objects = network_configuration.objects(length_1=6, length_2=50)
        assert objects["components"]["connectionAB"].subcomponents["channel_AtoB"].properties["length"] == 6
        assert objects["components"]["connectionBC"].subcomponents["channel_AtoB"].properties["length"] == 6
        assert objects["components"]["connectionCD"].subcomponents["channel_AtoB"].properties["length"] == 50

        # test that the functionality also works when iterating over the generator
        next_network_configuration = next(generator)
        config = next_network_configuration.config(length_1=80, length_2=42)
        self.assertEqual(80, config["components"]["connectionAB"]["properties"]["length"])
        self.assertEqual(80, config["components"]["connectionBC"]["properties"]["length"])
        self.assertEqual(42, config["components"]["connectionCD"]["properties"]["length"])
        self.assertEqual(0.1, config["components"]["connectionAB"]["properties"]["p_loss_init"])
        objects = network_configuration.objects(length_1=80, length_2=42)
        assert objects["components"]["connectionAB"].subcomponents["channel_AtoB"].properties["length"] == 80
        assert objects["components"]["connectionBC"].subcomponents["channel_AtoB"].properties["length"] == 80
        assert objects["components"]["connectionCD"].subcomponents["channel_AtoB"].properties["length"] == 42

    def test_config_snapshot(self):
        """
        Test that if the `save_path` param is given to `NetworkConfiguration.config()`,
        snapshots of the config dictionary are correctly saved as a YAML file.
        """
        generator = netconf_generator("./tests/test_configs/param.yaml",
                                      builders=[self.component_builder], linkers=[ComponentLinker()])
        network_configuration = next(generator)

        path_no_suffix = "./tests/test_configs/temporary_test_file"
        config = network_configuration.config(save_path=path_no_suffix, length_1=100, length_2=200)
        assert os.path.isfile(path_no_suffix + ".yaml")
        self.assertDictEqual(config, yaml.load(open(path_no_suffix + ".yaml", 'r'), Loader) or {})
        os.remove(path_no_suffix + ".yaml")

        path_suffix = path_no_suffix + ".yaml"
        config = network_configuration.config(save_path=path_suffix, length_1=10, length_2=20)
        assert os.path.isfile(path_suffix)
        self.assertDictEqual(config, yaml.load(open(path_suffix, 'r'), Loader) or {})
        os.remove(path_suffix)
