CHANGELOG
=========

2024-09-10 (3.1.0)
------------------
- Added `ChainBuilder` that makes it easy to build long (asymmetric) repeater chains.

2023-02-23 (3.0.0)
------------------
- Network configurations are now represented using the new `NetworkConfiguration` class.
- The `netconf_generator` function now returns a generator of `NetworkConfiguration` objects instead of dictionaries (those dictionaries can now be created using `NetworkConfiguration.objects()` and `NetworkConfiguration.config()`.
- Builders and linkers no longer use class methods but instead regular methods. The function `netconf_generator` therefore no longer takes builder or linker classes but instead objects.
- The function `netconf_generator` now takes the required arguments `builders` and `linkers` instead of the optional arguments `extra_builders` and `extra_linkers`.
- New special keyword that can be used by interacting with a `NetworkConfiguration` object directly to set some of the network parameters in Python: PARAM.
- Config dictionary of a `NetworkConfiguration` can be saved as a YAML file.

2021-11-12 (2.3.0)
------------------
- Added method for retrieving parameter set imported into a config file with the INCLUDE keyword.

2021-06-08 (2.2.0)
------------------
- UPDATE keyword can be defined on any nesting level. Before, it could be used only in the top level for each component.
- Added support for INCLUDE keyword which along with the !include construct imports parameters from external yaml file but does not overwrite any parameters that are already defined

2021-05-03 (2.1.2)
------------------
- RANGE and LIST keywords can now be combined with the UPDATE keyword in YAML config files.

2021-03-12 (2.1.1)
------------------
- Updated "home-page" in setup.cfg to the right gitlab instance (instead of the old gitlab.tudelft).
- New requirement: netsquid 0.9 as minimum version to account for the fact that netconf uses the Network component, which was only introduced in 0.9, and netsquid 2.0.0 as maximum, since it turns out that netconf is compatible with netsquid 1.0.0.
- Non-intrusive logger (using a separate logger for netsquid-netconf instead of global logging).

2020-12-16 (2.1.0)
------------------

- Support for SYNC keyword added to RANGE functionality to allow for synchronising values across specified components
- Added warnings for builder keys from `netconf_generator` that are not present in the YAML configuration file

2020-11-10 (2.0.0)
------------------

- `netconf_generator` now returns a tuple: dictionary with outputs from all the builders and a dictionary with the specific configuration parameters
- `NetworkBuilder` is called (i.e. network is created) only when YAML configuration file contains the key "network" (as opposed to previously used "network_name")
- Linkers no longer have a "key" associated with them, to distinguish them more clearly from builders and give more freedom
- Config file is now closed after reading
- Fixed a problem with UPDATE keyword (parameters were updated at every occurrence of the anchor)
- Improved documentation and included a tutorial of how to use this snippet

2020-06-29 (1.0.0)
------------------

- Reinstantiated with new snippet template
- Created a new configuration system using YAML config files, based on Builders and Linkers
- Allowed for UPDATE, LIST and RANGE keywords in YAML config files
- Added NetworkLinker which creates a NetSquid Network object
- Removed old easynetwork code

2019-06-27
----------

- Created this snippet
