NetSquid-NetConf (3.1.0)
================================

Description
-----------

This is a user contributed _snippet_ for the [NetSquid quantum network simulator](https://netsquid.org).

Snippet for network configuration.

Installation
------------

See the [INSTALL file](INSTALL.md) for instruction of how to install this snippet.

Documentation
-------------

So build and see the docs see the [docs README](docs/README.md).

Usage
-----

Uses so called _builders_ and _linkers_ to create and connect components to be used in a physical network.
Additionally, the `netconf_generator()` can be used to create multiple networks on the fly.

An example of a simple three node network can be found in the _examples_ folder, which can be executed with

``python3 examples/run_examples.py``

to see some printed output.

Contact
-------

For questions, contact the maintainer of this snippet: Guus Avis (g.avis@tudelft.nl)

Contributors
------------

Guus Avis (g.avis@tudelft.nl)
Wojciech Kozlowski
Hana Jirovská
Julian Rabbie
Francisco Silva
Axel Dahlberg
Matthew Skrzypczyk
Leon Wubben
Tim Coopmans
Stephanie Wehner

License
-------

The NetSquid-SnippetTemplate has the following license:

> Copyright 2018 QuTech (TUDelft and TNO)
> 
>   Licensed under the Apache License, Version 2.0 (the "License");
>   you may not use this file except in compliance with the License.
>   You may obtain a copy of the License at
> 
>     http://www.apache.org/licenses/LICENSE-2.0
> 
>   Unless required by applicable law or agreed to in writing, software
>   distributed under the License is distributed on an "AS IS" BASIS,
>   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>   See the License for the specific language governing permissions and
>   limitations under the License.
