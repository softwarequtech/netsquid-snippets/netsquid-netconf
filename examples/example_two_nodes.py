from netsquid_netconf.linker import NetworkLinker

from netsquid_netconf.netconf import netconf_generator
from netsquid_netconf.builder import ComponentBuilder, NetworkBuilder

from netsquid.nodes.connections import Connection


def main(no_output=False):

    component_builder = ComponentBuilder()
    component_builder.add_type("connection", Connection)
    builders = [component_builder, NetworkBuilder()]

    generator = netconf_generator("example_two_nodes.yaml", builders=builders, linkers=[NetworkLinker()])
    network_configuration = next(generator)
    builder_outputs = network_configuration.objects()
    config = network_configuration.config()

    assert builder_outputs.get("network", None) is not None
    components = builder_outputs.get("components", None)
    assert components is not None

    assert components.get("node_Alice", None) is not None
    assert components.get("node_Bob", None) is not None
    assert components.get("connection_Alice_to_Bob", None) is not None

    assert config.get("network", None) == "minimal_example"
    assert config.get("components", None) is not None
    assert len(config["components"]) == 3  # 2 nodes, 1 connection

    network = builder_outputs["network"]

    assert len(network.nodes) == 2
    assert len(network.connections) == 1
    for connection in network.connections.values():
        for port in connection.ports.values():
            assert port.is_connected

    if not no_output:
        print(f"Nodes: {network.nodes}")
        print(f"Connections: {network.connections}")


if __name__ == '__main__':
    main()
