from netsquid.components.cchannel import ClassicalChannel
from netsquid.components.models.delaymodels import FibreDelayModel
from netsquid.nodes.connections import DirectConnection
from netsquid_netconf.linker import NetworkLinker

from netsquid_netconf.builder import ComponentBuilder, NetworkBuilder
from netsquid_netconf.netconf import netconf_generator


class ClassicalFibreDirectConnection(DirectConnection):
    """A direct connection composed of symmetric classical fibres."""

    def __init__(self, name, **properties):
        super().__init__(
            name=name,
            channel_AtoB=ClassicalChannel(
                name="AtoB",
                models={"delay_model": FibreDelayModel()},
                **properties,
            ),
            channel_BtoA=ClassicalChannel(
                name="BtoA",
                models={"delay_model": FibreDelayModel()},
                **properties,
            ),
        )


def main(no_output=False):
    component_builder = ComponentBuilder()
    component_builder.add_type(
        "classical_fibre_direct_connection",
        ClassicalFibreDirectConnection,
    )
    builders = [component_builder, NetworkBuilder()]

    generator = netconf_generator("example_three_nodes.yaml", builders=builders, linkers=[NetworkLinker()])
    network_configuration = next(generator)
    objects = network_configuration.objects()

    assert objects.get("components", None) is not None
    assert objects.get("network", None) is not None

    # Check the output
    if not no_output:
        print(f"Components: {objects['components']}")
        print(f"Network: {objects['network']}")


if __name__ == '__main__':
    main()
