from netsquid.components.cchannel import ClassicalChannel
from netsquid.components.models.delaymodels import FibreDelayModel
from netsquid.nodes.connections import DirectConnection
from netsquid_netconf.linker import ComponentLinker, NetworkLinker

from netsquid_netconf.builder import ComponentBuilder, NetworkBuilder
from netsquid_netconf.netconf import netconf_generator


class ClassicalFibreDirectConnection(DirectConnection):
    """A direct connection composed of symmetric classical fibres."""

    def __init__(self, name, **properties):
        super().__init__(
            name=name,
            channel_AtoB=ClassicalChannel(
                name="AtoB",
                models={"delay_model": FibreDelayModel()},
                **properties,
            ),
            channel_BtoA=ClassicalChannel(
                name="BtoA",
                models={"delay_model": FibreDelayModel()},
                **properties,
            ),
        )


def main(no_output=False):
    component_builder = ComponentBuilder()
    component_builder.add_type(
        "classical_fibre_direct_connection",
        ClassicalFibreDirectConnection,
    )
    generator = netconf_generator("example_special_keywords.yaml", builders=[component_builder, NetworkBuilder()],
                                  linkers=[ComponentLinker(), NetworkLinker()])

    for index, network_configuration in enumerate(generator):

        config = network_configuration.config()
        objects = network_configuration.objects()
        assert objects.get("components", None) is not None
        assert objects.get("network", None) is not None

        # Check the output
        if not no_output:
            print(f"\n\nOutput #{index} from the generator:")
            # Connection0:
            #   length is varied due to RANGE keyword and synced with length of Connection1
            #   label of connect_to overwrites external parameters from INCLUDE keyword
            print(f"\nConfiguration of Connection0: {config.get('components', None).get('Connection0', None)}")
            # Connection1:
            #   length is varied due to RANGE keyword and synced with length of Connection0
            #   p_loss_init is varied due to LIST keyword
            #   label, node1, and node2 of connect_to overwrites external parameters from INCLUDE keyword
            print(f"\nConfiguration of Connection1: {config.get('components', None).get('Connection1', None)}")


if __name__ == '__main__':
    main()
