from netsquid_netconf.netconf import netconf_generator
from netsquid_netconf.builder import Builder
from netsquid_netconf.linker import Linker


class AddBuilder(Builder):

    def key(self):
        return "add"

    def build(self, config_dict):
        return config_dict["number_1"] + config_dict["number_2"]


class MultiplyBuilder(Builder):

    def key(self):
        return "multiply"

    def build(self, config_dict):
        return config_dict["number_1"] * config_dict["number_2"]


class DivideLinker(Linker):

    def link(self, object_dict, conf_dict):
        object_dict["multiply"] /= object_dict["add"]


def main(no_output=False):

    generator = netconf_generator("example_configuring_numbers.yaml",
                                  builders=[AddBuilder(), MultiplyBuilder()],
                                  linkers=[DivideLinker()])
    network_configuration = next(generator)

    # Check the output
    if not no_output:
        print(f"\nobjects: \n{network_configuration.objects()}")
        print(f"\nconfig: \n{network_configuration.config()}\n")


if __name__ == '__main__':
    main()
