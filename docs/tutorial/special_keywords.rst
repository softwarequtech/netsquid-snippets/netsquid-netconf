Special Keywords
----------------

NetSquid-NetConf uses some special keywords, which are interpreted in a special way when they are interpreted in YAML configuration files.
These keywords implement functionality that allows users to configure their networks with even more convenience.

Note that the examples in this section are simplified; components only have a `Connection <https://docs.netsquid.org/latest-release/api_nodes/netsquid.nodes.connections.html#netsquid.nodes.connections.Connection>`_ defined, and ports are not defined for the connection.


Anchors in YAML
^^^^^^^^^^^^^^^

To fully understand how the special keywords work, it is important to first illustrate the use of anchors in YAML.

Anchors can be used to define variables; first occurrence of data is marked as an anchor (using the `&` indicator) and all subsequent references (aliases) point to the anchor (using the `*` indicator).
More about anchors can be found in the `official YAML documentation <https://yaml.org/spec/1.2/spec.html>`_.
In NetSquid-NetConf YAML configuration files, an anchor can be used to define some baseline parameters that are repeated throughout the configuration.

This is how one would define properties of a node and use them to configure multiple nodes:

.. code-block:: YAML

    node: &node
      type: node
      properties:
        port_names:
          - A
          - B

    components:

      Node0:
        << : *node

      Node1:
        << : *node

UPDATE
^^^^^^

Whenever an anchor is used to define some default parameters, the UPDATE keyword can be used to overwrite only specific properties.
This update works recursively, UPDATE should be used at the level of parameters for a component (see the example).

The resulting connection from the following YAML file would have the following properties: length = 1, p_loss_init = 0, p_loss_length = 0.

.. code-block:: YAML

    fibre: &fibre
      type: classical_fibre_direct_connection
      properties:
        length: 0.002
        p_loss_init: 0
        p_loss_length: 0

    components:

      connection:
        << : *fibre
        UPDATE:
          properties:
            length: 1

INCLUDE
^^^^^^^

Sometimes, it might be desirable to include list of parameters from a different yaml file.
This can be done with the use of the !include construct and INCLUDE keyword. Below is a simplified example for defining fibre parameters with two YAML files.

`include_example.yaml`:

.. code-block:: YAML

    components:
      connection:
        type: classical_fibre_direct_connection
        properties:
          INCLUDE: !include fibre_params.yaml
          length: 1
          p_loss_init: 0

`fibre_params.yaml`:

.. code-block:: YAML

    node_params.yaml
        p_loss_init: 0.2
        p_loss_length: 0.2

In the example, the resulting properties of the fibre will be 1, 0 and 0.2 for `length`, `p_loss_init` and `p_loss_length` respectively.
Parameters from the included external file will not overwrite any already defined parameters.

The INCLUDE keyword can also be used within anchors.

LIST
^^^^

Used to configure a network with a number of different parameter values.
Each parameter value instantiates a network with a different value for the parameter for which LIST is used.
The :class:`~netsquid_netconf.netconf.NetworkConfiguration` objects corresponding to these different listed values are
obtained by iterating over the generator created by :func:`~netsquid_netconf.netconf.netconf_generator`.
The configuration dictionaries returned by :meth:`~netsquid_netconf.netconf.config` don't include the LIST keyword
but instead have a single value for the parameter, different for each object returned by the iterator.
More than one LIST keyword can be used in a file, the configurations will then be of all possible combinations of the different parameter values.

From the following example, five different configurations would be created, each one with a different value for length of the connection.

.. code-block:: YAML

    fibre: &fibre
      type: classical_fibre_direct_connection
      properties:
        length:
          LIST: [2, 4, 6, 8, 10]
        p_loss_init: 0
        p_loss_length: 0

    components:

      connection:
        << : *fibre

RANGE
^^^^^

Similarly to LIST, this keyword is used to configure a network with a number of different parameter values.
Each parameter value instantiates a network with a different value for the parameter for which RANGE is used.
The :class:`~netsquid_netconf.netconf.NetworkConfiguration` objects corresponding to these different listed values are
obtained by iterating over the generator created by :func:`~netsquid_netconf.netconf.netconf_generator`.
More than one RANGE keyword can be used in a file, the configurations will then be of all possible combinations of the different parameter values.
Note that one can also combine LIST and RANGE keywords within one YAML file.

RANGE exploration can be defined for integer-sized steps or for non-integer-sized steps.

In the following example, RANGE keyword has three parameters defined: START, STOP, STEP.
This internally calls the built-in range function from python, and so only works for integer-sized steps.
The following YAML file would create configurations that have the following values for the length parameter: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]. Note that the STOP value is not included.

.. code-block:: YAML

    fibre: &fibre
      type: classical_fibre_direct_connection
      properties:
        length:
          RANGE:
            START: 0
            STOP: 10
            STEP: 1
        p_loss_init: 0
        p_loss_length: 0

    components:

      connection:
        << : *fibre

To have non-integer-sized steps, one has to define NUM instead of STEP. Therefore, RANGE keyword would have these three parameters: START, STOP, NUM.
NUM defines how many values will be constructed within the START and STOP bounds.
Optionally, one can also define a Boolean for parameter ENDPOINT which specifies whether the value of STOP parameter will be included in the range. By default, this is True.
This configuration internally calls :func:`numpy.linspace`
For illustration, the following YAML file would create the same configurations as the other example using RANGE keyword.

.. code-block:: YAML

    fibre: &fibre
      type: classical_fibre_direct_connection
      properties:
        length:
          RANGE:
            START: 0
            STOP: 10
            NUM: 10
            ENDPOINT: False
        p_loss_init: 0
        p_loss_length: 0

    components:

      connection:
        << : *fibre

SYNC
^^^^

SYNC is a keyword used for additional configuration of RANGE.
As was explained above, if there are multiple RANGE keywords used in the configuration file, all possible combinations of corresponding values will be created.
It might, however, be desirable to sweep through a range of values while keeping some values equal (i.e. limit the number of possible combinations).
This can be done by adding a `SYNC: values` key-value pair to chosen RANGE keywords. (The number of possible values for all RANGE keywords with SYNC should be equal.)

Take the following architecture as an example:

.. code-block::

     -------                         -------                         -------                         -------
    | NodeA |     -------------     | NodeB |     -------------     | NodeC |     -------------     | NodeD |
    | B   A | <> | ConnectionL | <> | B   A | <> | ConnectionM | <> | B   A | <> | ConnectionR | <> | B   A |
    |       |     -------------     |       |     -------------     |       |     -------------     |       |
     -------                          -------                         -------                        -------

For each of the three connections, we can define its length. Say that we want to sweep through values [10, 20, 30] but we always want to keep the length of `ConnectionL` and `ConnectionR` equal.
The yaml configuration file would then look like this (definition of nodes is omitted):

.. code-block:: YAML

    fibre: &fibre
      type: classical_fibre_direct_connection
      properties:
        length:
          RANGE:
            START: 10
            STOP: 40
            STEP: 10
            SYNC: values
        p_loss_init: 0
        p_loss_length: 0

    components:

      connectionL:
        << : *fibre

      connectionM:
        << : *fibre
        UPDATE:
          properties:
            length:
              RANGE:
                START: 10
                STOP: 40
                STEP: 10

      connectionR:
        << : *fibre

The configuration file specifies that the values for the left and right connection should be always kept equal; it creates the following configurations, shown in the order (ConnectionL, ConnectionM, ConnectionR):

(10, 10, 10), (10, 20, 10), (10, 30, 10), (20, 10, 20), (20, 20, 20), (20, 30, 20), (30, 10, 30), (30, 20, 30), (30, 30, 30)

If the SYNC keyword was included for all the connections in the example above, there would simply be three resulting configurations: (10, 10, 10), (20, 20, 20), (30, 30, 30).

Note that SYNC can take on any String value (in the example above, it is simply "values"), but it should be uniform for all the SYNC keywords in one configuration file.
Having multiple separate SYNC configurations within one file is not yet supported.

PARAM
^^^^^

The PARAM keyword can be used to leave some of the parameters in the network configuration free to be defined on the go.
These parameters are given their values in python instead of in YAML,
and these values are only defined after writing the configuration file and calling the netconf generator.
Using the PARAM keyword, the YAML config file is effectively used to create a function that creates networks from
parameter values.
The PARAM keyword is used to define the name of a parameter in the config file as in this example:

.. code-block:: YAML

    fibre: &fibre
      type: classical_fibre_direct_connection
      properties:
        length:
          PARAM: length
        p_loss_init: 0
        p_loss_length: 0

    components:

      connection:
        << : *fibre

The :class:`~netsquid_netconf.netconf.NetworkConfiguration` object created from this YAML file will then take the
keyword argument `length` when creating a configuration dictionary using
:meth:`~netsquid_netconf.netconf.NetworkConfiguration.config` or when building and
linking the objects in the network using :meth:`~netsquid_netconf.netconf.NetworkConfiguration.objects`.
Let `network_configuration_1` be the :class:`~netsquid_netconf.netconf.NetworkConfiguration`
generated using the above YAML file.
Let `network_configuration_2` be the object generated using

.. code-block:: YAML

    fibre: &fibre
      type: classical_fibre_direct_connection
      properties:
        length: 100
        p_loss_init: 0
        p_loss_length: 0

    components:

      connection:
        << : *fibre

Then, the networks created by `network_configuration_1.objects(length=100)` and `network_configuration_2.objects()`
are the same.
The same holds for the configuration dictionaries; `network_configuration_1.config(length=100)` and
`network_configuration_2.config()` give the same dictionary.
However, if we now instead want to study the same network but with a length of 200, we can simply call
`network_configuration_1.objects(length=200)` instead of writing a new YAML file.
This can be useful when making decisions on which part of the parameter space to investigate based on previous
simulations, for example when performing parameter optimization.
In that case, the LIST and RANGE keyword are not suitable as in those cases the set of parameters needs to be specified
a priori.
Note that multiple free parameters can be defined using the PARAM keyword, and one free parameter can control
the value of multiple network properties.
For example:

.. code-block:: YAML

    components:
        connection1:
            type: classical_fibre_direct_connection
            properties:
                length:
                    PARAM: length_1
        connection2:
            type: classical_fibre_direct_connection
            properties:
                length:
                    PARAM: length_1
        connection3:
            type: classical_fibre_direct_connection
            properties:
                length:
                    PARAM: length_2

Then, using the netconf generator to obtain the corresponding `network_configuration` object, the configuration
dictionary created by `network_configuration.config(length_1=100, length_2=200)` is equivalent to the one generated
without PARAM keyword with the YAML file

.. code-block:: YAML

    components:
        connection1:
            type: classical_fibre_direct_connection
            properties:
                length: 100
        connection2:
            type: classical_fibre_direct_connection
            properties:
                length: 100
        connection3:
            type: classical_fibre_direct_connection
            properties:
                length: 200
