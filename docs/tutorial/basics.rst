Basics
------

Here, we explain the basic functionality of NetSquid-NetConf.
NetSquid-NetConf can read a configuration file written in YAML to obtain a
:class:`~netsquid_netconf.netconf.NetworkConfiguration` object.
This object holds the network that was constructed based on the YAML file as
:meth:`~netsquid_netconf.netconf.NetworkConfiguration.objects`.


Builders and Linkers
^^^^^^^^^^^^^^^^^^^^

YAML configuration files are interpreted by builders and linkers, and therefore builders and linkers form the foundation of NetSquid-NetConf.

Builders are used to construct python objects according to a specific recipe (which is specified in the :meth:`~netsquid_netconf.builder.Builder.build` method of each specific builder).
They do so using a dictionary as input.
This dictionary is retrieved from the configuration file, using the :meth:`~netsquid_netconf.builder.Builder.key` method of the specific builder.
They output a collection of objects.
Typically, these objects are NetSquid `Components <https://docs.netsquid.org/latest-release/api_nodes/netsquid.nodes.network.html#netsquid.components.component.Component>`__.

Linkers act on the objects constructed by builders, after all the builders are finished.
This typically means connecting or linking them together in one way or another, such that the individual objects are combined into a network.
How linkers act exactly is specified in their respective :meth:`~netsquid_netconf.linker.Linker.link` methods, which use as input both a dictionary of components constructed by builders and everything included in the configuration file.

As an example, we here define two mock builders and one mock linker.
They cannot be used for configuring networks.
Instead, for the purpose of demonstrating the basic principles of NetSquid-NetConf, they 'configure' numbers.
First, we define two builders: one that adds two numbers together, and one that multiplies two numbers together.

.. code-block:: python

    from netsquid_netconf.builder import Builder


    class AddBuilder(Builder):

        def key(self):
            return "add"

        def build(self, config_dict):
            return config_dict["number_1"] + config_dict["number_2"]


    class MultiplyBuilder(Builder):

        def key(self):
            return "multiply"

        def build(self, config_dict):
            return config_dict["number_1"] * config_dict["number_2"]


Secondly, we define a linker that 'links' the results from the AddBuilder and the MultiplyBuilder by dividing the result of the MultiplyBuilder by the result of the AddBuilder.

.. code-block:: python

    from netsquid_netconf.linker import Linker
    
    
    class DivideLinker(Linker):

        def link(self, object_dict, conf_dict):
            object_dict["multiply"] /= object_dict["add"]

The definitions of the builders and linker above can also be found in 'example_configuring_numbers.py' in the 'examples' folder of NetSquid-NetConf.


How to Write a Config File
^^^^^^^^^^^^^^^^^^^^^^^^^^

Configuration files for NetSquid-NetConf are written using YAML.
For details about the syntax of YAML in general, see e.g. `Wikipedia <https://en.wikipedia.org/wiki/YAML#Syntax>`_.
A configuration file needs, at its highest level, to consist of key-value pairs, where keys are the :meth:`~netsquid_netconf.builder.Builder.key`\s corresponding to specific builders and the values encode dictionaries passed on to the respective builder.
What such a dictionary should look like, depends both on the builder that has to interpret it and on the linkers that are being used (linkers may e.g. use the content of these dictionaries to decide what components should be connected together).

If we want to write a YAML configuration file for configuring numbers using the builders and linker defined above, this would look as follows.

.. code-block:: YAML

    add:
        number_1: 12
        number_2: 8

    multiply:
        number_1: 4
        number_2: 25

A YAML file with this code can also be found in 'example_configuring_numbers.yaml' in the 'examples' folder of NetSquid-NetConf.


How to Compile
^^^^^^^^^^^^^^

In order to turn a YAML configuration file into a usable network, you need to use :func:`~netsquid_netconf.netconf.netconf_generator`.
In this function, you can specify the name of the configuration file you want to interpret.
Furthermore, you need to specify the builders and linkers that should be used.

As output, :func:`~netsquid_netconf.netconf.netconf_generator` will return a generator.
When :func:`~netsquid_netconf.netconf.netconf_generator.next` is called on the generator,
a :class:`~netsquid_netconf.netconf.NetworkConfiguration` is returned.
This class has several important methods and properties.
First of all, the builders and linkers that were used to create the object can be found using
:attr:`~netsquid_netconf.netconf.NetworkConfiguration.builders` and
:attr:`~netsquid_netconf.netconf.NetworkConfiguration.linkers`.
A dictionary holding the configuration parameters can be found using
:meth:`~netsquid_netconf.netconf.NetworkConfiguration.config`.
For basic YAML files, this will simply be a dictionary with all the properties defined in the YAML file.
But perhaps most important method is :meth:`~netsquid_netconf.netconf.NetworkConfiguration.objects`;
when this method is called, the builders and linkers are called to actually construct the network,
and the constructed objects are returned in the form of a dictionary.
This dictionary has as keys the :meth:`~netsquid_netconf.builder.Builder.key`\s of the used builders.
Under these keys, the output of the respective :meth:`~netsquid_netconf.builder.Builder.build` methods can be found (after the linkers have acted on them).

If we want to use the YAML configuration defined above to configure numbers with AddBuilder, MultiplyBuilder and DivideLinker, the compilation looks as follows, assuming we saved the configuration under the name 'example_configuring_numbers.yaml'.

.. code-block:: python

   >>> from netsquid_netconf.netconf import netconf_generator
    
   >>> generator = netconf_generator("example_number_builders.yaml",
   >>>                               extra_builders=[AddBuilder(), MultiplyBuilder()],
   >>>                               extra_linkers=[DivideLinker()])
   >>> network_configuration = next(generator)
   >>> print(f"\nobjects: \n{network_configuration.objects()}")
   >>> print(f"\nconfig: \n{network_configuration.config()}\n")
   
   objects: 
   {'add': 20, 'multiply': 5.0}

   config: 
   {'add': {'number_1': 12, 'number_2': 8}, 'multiply': {'number_1': 4, 'number_2': 25}}

This compilation step can also be found in 'example_configuring_numbers.py' in the 'examples' folder of NetSquid-NetConf.
