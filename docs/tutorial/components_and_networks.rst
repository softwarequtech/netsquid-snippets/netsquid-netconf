Components and Networks
-----------------------

Currently the probably most-used way of building networks with NetSquid-NetConf is using :class:`~netsquid_netconf.builder.ComponentBuilder`, :class:`~netsquid_netconf.builder.NetworkBuilder` and :class:`~netsquid_netconf.linker.NetworkLinker`.
Therefore, we will explain how these can be used in conjunction here in more detail, and show an example.

ComponentBuilder
^^^^^^^^^^^^^^^^

:class:`netsquid_netconf.builder.ComponentBuilder` is a specific builder used to build generic NetSquid `Components <https://docs.netsquid.org/latest-release/api_nodes/netsquid.nodes.network.html#netsquid.components.component.Component>`__. 
Its :meth:`~netsquid_netconf.builder.ComponentBuilder.key` is 'components'. 
Thus, if the key 'components' is used in a YAML configuration file, the corresponding value will be passed to the builder's :meth:`~netsquid_netconf.builder.ComponentBuilder.build` method.
This value should be a dictionary with the names of the components you want to instantiate as keys.
The name is given to the component upon its construction and can be used to retrieve the component from the dictionary obtained using
:meth:`~netsquid_netconf.netconf.network_configuration.objects`, but is otherwise of no significance.

The value corresponding to each name should in turn be a dictionary.
This dictionary needs to at least have the key 'type', with as value a string corresponding to a specific type of component.
The specified component needs to be known to the used instance of ComponentBuilder (more about this below).
Furthermore, there can be the key 'properties'.
This should have, as value, a dictionary with parameter names as keys, and parameter values as values.
These parameters are passed on to the component upon construction (i.e. they are given as keyword arguments in the __init__() of the respective component).
Lastly, there can be the keyword 'components'.
This can be used to specify subcomponents, and is interpreted by the ComponentBuilder in a recursive manner.

Consider for example the following YAML configuration file to configure a single component with the name 'my_component' and the type 'my_type'. 
When constructing the component, it is given the argument 'my_property' with value 'my_value'. 

.. code-block:: YAML

    components:
        my_component:
            type: my_type
            properties:
                my_property: my_value

A ComponentBuilder object can only instantiate a component if it knows the 'type' specified for it.
Internally, the object has a dictionary :meth:`~netsquid_netconf.builder.ComponentBuilder.types` which matches 'type' keywords to classes (which are all subclasses of NetSquid's `Component <https://docs.netsquid.org/latest-release/api_nodes/netsquid.nodes.network.html#netsquid.components.component.Component>`__).
If the 'type' is known, i.e. in the dictionary, the corresponding class is used to construct the required component.
This is done using the parameter values defined under the 'properties' key as keyword arguments, and using the key used to define the component in the YAML config file as value for the 'name' argument.
By default, ComponentBuilder only knows the type 'node', which maps to NetSquid's `Node <https://docs.netsquid.org/latest-release/api_nodes/netsquid.nodes.network.html#netsquid.nodes.node.Node>`__ class.
More types can be added using the :meth:`~netsquid_netconf.builder.ComponentBuilder.add_type` method, which must be called after instantiating the builder object but before reading a YAML configuration file.

For example, before we would be able to use the above YAML configuration for setting up 'my_component', we would need to tell ComponentBuilder how to interpret type 'my_type'.
This could e.g. be done as follows.

.. code-block:: python

    from netsquid_netconf.builder import ComponentBuilder
    from netsquid.components.component import Component


    class MyType(Component)
        def __init__(self, name, my_property)
            super().__init__(self, name)
            self.my_property = my_property

    component_builder = ComponentBuilder()
    component_builder.add_type("my_type", MyType)


NetworkBuilder and NetworkLinker
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

:class:`netsquid_netconf.builder.NetworkBuilder` is a builder which creates a NetSquid `Network <https://docs.netsquid.org/latest-release/api_nodes/netsquid.nodes.network.html#netsquid.nodes.network.Network>`_ object.
Its :meth:`~netsquid-netconf.builder.NetworkBuilder.key` is 'network', and the value that the builder expects is a string to be used as name for the network.
The only thing that NetworkBuilder does is construct an empty network, with the specified value as name.
If the key 'network' is not defined within the YAML file, neither :class:`netsquid_netconf.builder.NetworkBuilder` nor :class:`netsquid_netconf.linker.NetworkLinker` will be called, even if these builders are included.
So to construct a network with the name 'my_network', a YAML configuration file only needs to contain the following.

.. code-block:: YAML
    
    network: my_network

:class:`netsquid_netconf.linker.NetworkLinker` is a linker which populates the empty network created by NetworkBuilder with  NetSquid `Node <https://docs.netsquid.org/latest-release/api_nodes/netsquid.nodes.node.html#netsquid.nodes.node.Node>`__ objects and NetSquid `Connection <https://docs.netsquid.org/latest-release/api_nodes/netsquid.nodes.connections.html#netsquid.nodes.connections.Connection>`_ objects.
To add nodes and connections to the network, nodes and connections need to be constructed using builders, e.g. :class:`~netsquid_netconf.builder.ComponentBuilder`.
All nodes that are defined this way are automatically added to the network by NetworkLinker.
On the other hand, connections that should be included in the network need to have the 'connect_to' key in the dictionary defining them. 
When using ComponentBuilder, this key occurs at the same level in the YAML configuration file as the keys 'type' and 'properties'.

Any connection with the 'connect_to' keyword will be added to the network using the `add\_connection <https://docs.netsquid.org/latest-release/api_nodes/netsquid.nodes.network.html#netsquid.nodes.network.Network.add_connection>`__ method of the network instantiated by NetworkBuilder.
The value corresponding to the key 'connect_to' is a dictionary defining keyword arguments passed on the the `add\_connection <https://docs.netsquid.org/latest-release/api_nodes/netsquid.nodes.network.html#netsquid.nodes.network.Network.add_connection>`__, together with the keyword argument 'connection', which has a value the connection object as created by the builders.
This must at least include the keyword arguments 'node1' and 'node2', which should have as values the names of the nodes that should be connected by the connection.
Additionally, it is often useful to include keyword arguments 'port_name_node1' and 'port_name_node2', with the names of the ports that should be used to connect the nodes as values.

So if there are two nodes, 'node_Alice' and 'node_Bob', they can be connected by including the following in the 'components' section of the YAML configuration file.

.. code-block:: YAML

    my_connection:
        type: connection
        connect_to:
            node1: node_Alice
            node2: node_Bob

Note that when there are no nodes constructed using :class:`~netsquid_netconf.builder.ComponentBuilder`, the network will be empty. Similarly, if no connections are defined using the 'connect_to' keyword, the network will only hold nodes constructed by :class:`~netsquid_netconf.builder.ComponentBuilder`.

Example
^^^^^^^

Here we consider a full example where a simple network of two nodes is configured using :class:`~netsquid_netconf.builder.NetworkBuilder`, :class:`~netsquid_netconf.builder.ComponentBuilder` and :class:`~netsquid_netconf.linker.NetworkLinker`.
Specifically, we use NetworkBuilder to create an empty network, ComponentBuilder to instantiate two Node objects and a `Connection <https://docs.netsquid.org/latest-release/api_nodes/netsquid.nodes.connections.html#netsquid.nodes.connections.Connection>`_ object, and we use NetworkLinker to put them into the network.

In our configuration file, which we save under the name 'minimal_example.yaml', we can then have the following code:

.. code-block:: YAML

    network: minimal_example
 
    components:
      node_Alice:
        type: node
        properties:
          port_names:
            - port_to_Bob

      node_Bob:
        type: node
        properties:
          port_names:
            - port_to_Alice

      connection_Alice_to_Bob:
        type: connection
        connect_to:
          node1: node_Alice
          node2: node_Bob
 
Additionally, we need to have a python file which tells :class:`~netsquid_netconf.builder.ComponentBuilder` to interpret the type 'connection' as NetSquid's Connection class, and calls :func:`~netsquid_netconf.netconf.netconf_generator` to interpret the YAML file.

.. code-block:: python

   >>> from netsquid_netconf.netconf import netconf_generator
   >>> from netsquid_netconf.builder import ComponentBuilder, NetworkBuilder
   >>> from netsquid_netconf.linker import NetworkLinker
 
   >>> from netsquid.nodes.connections import Connection

   >>> component_builder = ComponentBuilder()
   >>> component_builder.add_type("connection", Connection)
   >>> builders = [component_builder, NetworkBuilder()]
 
   >>> generator = netconf_generator("example_two_nodes.yaml", builders=builders, linkers=[NetworkLinker()])
   >>> network_configuration = next(generator)
   >>> network = network_configuration.objects()["network"]
   >>> print(f"Nodes: {network.nodes}")
   >>> print(f"Connections: {network.connections}")
 
   Nodes: ConstrainedMapView({'node_Alice': Node(name='node_Alice'), 'node_Bob': Node(name='node_Bob')})
   Connections: ConstrainedMapView({'conn|node_Alice<->node_Bob|': Connection(name='connection_Alice_to_Bob')})
 
This example can also be found in the 'examples' folder of NetSquid-NetConf.
The YAML configuration file has the name 'example_two_nodes.yaml', while the python file has the name 'example_two_nodes.py'.
Additionally, there is also a three-node example in this folder that is slightly more complicated.
