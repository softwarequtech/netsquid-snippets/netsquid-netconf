API Documentation
-----------------

Below are the modules of this package.
`netconf` is the main module of netsquid-netconf and contains tools required to read YAML configuration files.
`builder` contains builders, which are used to build specific objects, based on the contents of a YAML file.
`linker` contains linkers, which are used to connect objects built by builders, based on the contents of a YAML file.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/netconf.rst

   modules/builder.rst

   modules/linker.rst   
