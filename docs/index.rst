.. NetSquid-NetConf documentation master file, created by
   sphinx-quickstart on Thu Oct 17 09:49:36 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NetSquid-NetConf's documentation!
===========================================================

This package is a snippet to be used with **NetSquid**, the **Net**\work **S**\imulator for **Qu**\antum **I**\nformation using **D**\iscrete events.
Using NetSquid-NetConf, users can configure physical quantum networks within NeSquid using YAML configuration files.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   api_tutorial
   api_modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
